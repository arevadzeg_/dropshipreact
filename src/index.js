import React from "react";
import ReactDOM from "react-dom";
import "./index.css";
import App from "./App";
import reportWebVitals from "./reportWebVitals";
import { BrowserRouter } from "react-router-dom";
import { createMuiTheme, ThemeProvider } from "@material-ui/core/styles";
import { SnackbarProvider } from "notistack";
import { createStore, combineReducers } from "redux";
import productsReducer from "./components/Redux/ProductsReducer/productsReducer";
import adminReducer from "./components/Redux/AdminReducer/adminReducer";
import { Provider } from "react-redux";

const theme = createMuiTheme({
  palette: {
    primary: {
      main: "#61d5df",
      contrastText: "#fff",
    },
  },
  typography: {
    button: {
      fontWeight: "800",
      fontSize: "0.70em",
      paddingTop: "7px",
      paddingBottom: "7px",
      paddingRight: "10px",
      paddingLeft: "10px",
    },
  },
});

const store = createStore(
  combineReducers({
    products: productsReducer,
    admin: adminReducer,
  }),
  window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__()
);

ReactDOM.render(
  <BrowserRouter>
    <ThemeProvider theme={theme}>
      <Provider store={store}>
        <SnackbarProvider>
          <App />
        </SnackbarProvider>
      </Provider>
    </ThemeProvider>
  </BrowserRouter>,
  document.getElementById("root")
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
