import "./nav.css";
import NavItem from "./NavItem";
import { Link, useLocation } from "react-router-dom";
import { useDispatch } from "react-redux";
import { resetStore } from "../Redux/ProductsReducer/productsActions";

const Nav = () => {
  const dispatch = useDispatch();
  const resetEverything = () => {
    dispatch(resetStore());
  };

  const location = useLocation();
  const pathname = location.pathname;

  return (
    <div className="nav">
      <Link to="/" className="logo">
        <div className="nav__logo">
          <img
            alt=""
            src="https://app.365dropship.com/assets/images/dropship_logo.png"
          />
        </div>
      </Link>

      <Link to="profile">
        <div className="nav__profile">
          <img
            src="https://app.365dropship.com/assets/images/profile-example.jpg"
            alt=""
          />
        </div>
      </Link>

      <Link to="/dashboard">
        <NavItem className="nav__dashboard" icon="fa-dice-d20" />
      </Link>

      <Link to="/catalog">
        <NavItem
          className="nav__catalog"
          icon="fa-address-card"
          resetEverything={resetEverything}
          location="/catalog"
          pathname={pathname}
        />
      </Link>
      <Link to="/inventory">
        <NavItem
          className="nav__invenory"
          icon="fa-dice-d6"
          resetEverything={resetEverything}
          location="/inventory"
          pathname={pathname}
        />
      </Link>

      <Link to="/cart">
        <NavItem className="nav__cart" icon="fa-shopping-cart" />
      </Link>

      <Link to="/orders">
        <NavItem className="nav__orders" icon="fa-clipboard-check" />
      </Link>

      <Link to="/transactions">
        <NavItem className="nav__transactions" icon="fa-clipboard-list" />
      </Link>

      <Link to="/store">
        <NavItem className="nav__store-list" icon="fa-exchange-alt" />
      </Link>
    </div>
  );
};

export default Nav;
