import "./NavItem.css";
import { useDispatch } from "react-redux";
import { resetStore } from "../Redux/ProductsReducer/productsActions";
import { useLocation } from "react-router-dom";

function NavItem({ className, icon, resetEverything, location }) {
  const dispatch = useDispatch();

  const x = useLocation();
  const pathname = x.pathname;

  const handleCleanUp = () => {
    if (pathname !== location) {
      dispatch(resetStore());
    }
  };

  return (
    <div className={className} onClick={handleCleanUp}>
      <i className={`fas ${icon}`}></i>
    </div>
  );
}

export default NavItem;
