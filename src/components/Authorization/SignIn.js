import { Checkbox } from "@material-ui/core";
import { useState } from "react";
import { Typography, Button, InputAdornment } from "@material-ui/core";
import { Box, TextField } from "@material-ui/core";
import EmailIcon from "@material-ui/icons/Email";
import VpnKeyIcon from "@material-ui/icons/VpnKey";
import VisibilityIcon from "@material-ui/icons/Visibility";
import VisibilityOffIcon from "@material-ui/icons/VisibilityOff";
import { useFormik } from "formik";
import { Link } from "react-router-dom";
import { handleLogIn, checkTokenValidity } from "../API/api";
import { useEffect } from "react";
import { signInValidationSchema as validationSchema } from "./Validations";
import { Alert } from "@material-ui/lab";
import useStyles from "./singInStyles";
import { useHistory } from "react-router";

const LogIn = () => {
  const [passwordVisible, setPasswordVisible] = useState(false);
  const [loadLoginGif, setLoadLoginGif] = useState(false);
  const [passwordIncorect, sePasswordIncorect] = useState("");

  const history = useHistory();

  useEffect(() => {
    checkTokenValidity().then((res) => {
      if (res) history.push("/catalog");
    });
  }, [history]);

  const formik = useFormik({
    initialValues: {
      email: "",
      password: "",
    },
    onSubmit: () => {
      setLoadLoginGif(true);
      const data = {
        email: formik.values.email,
        password: formik.values.password,
      };
      handleLogIn(data)
        .then((res) => localStorage.setItem("password", formik.values.password))
        .catch((err) => {
          if (err.response.status === 403) {
            sePasswordIncorect("Email or Password is incorrect");
          }
          setLoadLoginGif(false);
        });
    },
    validationSchema,
  });

  const changePasswordVisibility = () => {
    setPasswordVisible(!passwordVisible);
  };

  const classes = useStyles();
  return (
    <Box>
      <form onSubmit={formik.handleSubmit}>
        {loadLoginGif ? (
          <Box>
            <img
              src="https://app.365dropship.com/365Loader.b07204a17f483c38bc09.gif"
              alt="img"
            />
          </Box>
        ) : (
          <>
            <Box className={classes.logInHeader} mb={3}>
              <img
                src={"https://app.365dropship.com/assets/images/auth/logo.svg"}
                alt="img"
              />
              <Box ml={10}>Members Log In</Box>
            </Box>

            <Box className={classes.logInInputs}>
              {passwordIncorect ? (
                <Alert severity="error">{passwordIncorect}</Alert>
              ) : (
                ""
              )}
              <TextField
                variant="outlined"
                onChange={formik.handleChange}
                name="email"
                placeholder="Email"
                onBlur={formik.handleBlur}
                error={formik.touched.email && formik.errors.email}
                helperText={formik.touched.email && formik.errors.email}
                InputProps={{
                  autoComplete: "on",
                  startAdornment: (
                    <InputAdornment position="start">
                      <EmailIcon />
                    </InputAdornment>
                  ),
                }}
              />
              <TextField
                type={passwordVisible ? "text" : "password"}
                variant="outlined"
                placeholder="Password"
                name="password"
                onChange={formik.handleChange}
                onBlur={formik.handleBlur}
                error={formik.touched.password && formik.errors.password}
                helperText={formik.touched.password && formik.errors.password}
                InputProps={{
                  autoComplete: "on",
                  startAdornment: (
                    <InputAdornment position="start">
                      <VpnKeyIcon />
                    </InputAdornment>
                  ),
                  endAdornment: (
                    <InputAdornment position="end">
                      <Checkbox
                        onClick={changePasswordVisibility}
                        icon={<VisibilityOffIcon />}
                        checkedIcon={<VisibilityIcon />}
                      ></Checkbox>
                    </InputAdornment>
                  ),
                }}
              />
              <Typography className={classes.centerText}>
                Forgot Password
              </Typography>
              <Button
                color="primary"
                variant="contained"
                disableElevation
                type="submit"
              >
                Log In
              </Button>

              <Typography className={classes.centerText}>
                Or Log in With
              </Typography>
              <Box className={classes.logInOnptions}>
                <Button variant="contained" color="secondary" disableElevation>
                  Google
                </Button>
                <Button variant="contained" color="secondary" disableElevation>
                  Facebook
                </Button>
              </Box>

              <Typography className={classes.centerText}>
                Don't have an account <Link to="/signup">Sign Up</Link>
              </Typography>
            </Box>
          </>
        )}
      </form>
    </Box>
  );
};

export default LogIn;
