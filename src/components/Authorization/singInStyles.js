import { makeStyles } from "@material-ui/core";

const useStyles = makeStyles({
  logInHeader: {
    display: "flex",
  },
  logInInputs: {
    display: "flex",
    flexDirection: "column",
    gap: "25px",
  },
  logInOnptions: {
    display: "flex",
    justifyContent: "space-around",
  },

  centerText: {
    textAlign: "center",
  },
});

export default useStyles;
