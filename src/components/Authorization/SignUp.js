import {
  TextField,
  Button,
  InputAdornment,
  Box,
  makeStyles,
  Checkbox,
  FormControlLabel,
} from "@material-ui/core";
import EmailIcon from "@material-ui/icons/Email";
import PersonIcon from "@material-ui/icons/Person";
import VpnKeyIcon from "@material-ui/icons/VpnKey";
import { useFormik } from "formik";
import "./registration.css";
import { Typography } from "@material-ui/core";
import { Link } from "react-router-dom";
import { useState } from "react";
import VisibilityIcon from "@material-ui/icons/Visibility";
import VisibilityOffIcon from "@material-ui/icons/VisibilityOff";
import { handleRegistration, checkTokenValidity } from "../API/api";
import { useEffect } from "react";
import { signUpValidationSchema as validationSchema } from "./Validations";
import { useHistory } from "react-router";

const useStyles = makeStyles({
  signUpHeader: {
    display: "flex",
  },
});

const Profile = () => {
  const classes = useStyles();

  const history = useHistory();
  useEffect(() => {
    checkTokenValidity().then((res) => {
      if (res) history.push("/catalog");
    });
  }, [history]);

  const [showPassword, setShowPassword] = useState(false);
  const [showPasswordConfirmation, setShowPasswordConfirmation] =
    useState(false);

  const handleShowPassword = () => {
    setShowPassword(!showPassword);
  };

  const handleShowPasswordConfirmation = () => {
    setShowPasswordConfirmation(!showPasswordConfirmation);
  };
  const formik = useFormik({
    initialValues: {
      name: "",
      lastName: "",
      email: "",
      password: "",
      passwordConformation: "",
    },
    validationSchema,

    onSubmit: () => {
      const data = {
        firstName: formik.values.name,
        lastName: formik.values.lastName,
        email: formik.values.email,
        password: formik.values.password,
        passwordConfirmation: formik.values.passwordConformation,
      };

      handleRegistration(data)
        .then((res) => localStorage.setItem("password", formik.values.password))
        .catch();
    },
  });

  return (
    <div>
      <Box mb={3} className={classes.signUpHeader}>
        <img
          src={"https://app.365dropship.com/assets/images/auth/logo.svg"}
          alt="img"
        />

        <Box ml={10}>
          <Typography>Sign Up</Typography>
        </Box>
      </Box>

      <form className="registrationform" onSubmit={formik.handleSubmit}>
        <TextField
          variant="outlined"
          name="name"
          onChange={formik.handleChange}
          value={formik.values.name}
          placeholder="First Name"
          error={formik.touched.name && formik.errors.name}
          helperText={formik.touched.name && formik.errors.name}
          onBlur={formik.handleBlur}
          InputProps={{
            startAdornment: (
              <InputAdornment position="start">
                <PersonIcon />
              </InputAdornment>
            ),
          }}
        />

        <TextField
          variant="outlined"
          placeholder="Last Name"
          name="lastName"
          onChange={formik.handleChange}
          value={formik.values.lastName}
          error={formik.touched.lastName && formik.errors.lastName}
          helperText={formik.touched.lastName && formik.errors.lastName}
          onBlur={formik.handleBlur}
          InputProps={{
            startAdornment: (
              <InputAdornment position="start">
                <PersonIcon />
              </InputAdornment>
            ),
          }}
        />
        <TextField
          variant="outlined"
          placeholder="Email Address"
          name="email"
          onChange={formik.handleChange}
          value={formik.values.email}
          error={formik.touched.email && formik.errors.email}
          helperText={formik.touched.email && formik.errors.email}
          onBlur={formik.handleBlur}
          InputProps={{
            startAdornment: (
              <InputAdornment position="start">
                <EmailIcon />
              </InputAdornment>
            ),
          }}
        />
        <TextField
          variant="outlined"
          placeholder="Password"
          type={showPassword ? "text" : "password"}
          name="password"
          onChange={formik.handleChange}
          value={formik.values.password}
          error={formik.touched.password && formik.errors.password}
          helperText={formik.touched.password && formik.errors.password}
          onBlur={formik.handleBlur}
          InputProps={{
            startAdornment: (
              <InputAdornment position="start">
                <VpnKeyIcon />
              </InputAdornment>
            ),
            endAdornment: (
              <InputAdornment position="end">
                <Checkbox
                  onClick={handleShowPassword}
                  icon={<VisibilityOffIcon />}
                  checkedIcon={<VisibilityIcon />}
                ></Checkbox>
              </InputAdornment>
            ),
          }}
        />
        <TextField
          variant="outlined"
          placeholder="Confirm Password"
          type={showPasswordConfirmation ? "text" : "password"}
          name="passwordConformation"
          onChange={formik.handleChange}
          value={formik.values.passwordConformation}
          error={
            formik.touched.passwordConformation &&
            formik.errors.passwordConformation
          }
          helperText={
            formik.touched.passwordConformation &&
            formik.errors.passwordConformation
          }
          onBlur={formik.handleBlur}
          InputProps={{
            startAdornment: (
              <InputAdornment position="start">
                <VpnKeyIcon />
              </InputAdornment>
            ),
            endAdornment: (
              <InputAdornment position="end">
                <Checkbox
                  onClick={handleShowPasswordConfirmation}
                  icon={<VisibilityOffIcon />}
                  checkedIcon={<VisibilityIcon />}
                ></Checkbox>
              </InputAdornment>
            ),
          }}
        />
        <Box>
          <Typography>
            By creating an account, you agree with the Terms & Conditions and
            Privacy Policy
          </Typography>
          <FormControlLabel
            control={<Checkbox color="secondary" />}
            label="Subscribe to Newsletter"
            labelPlacement="end"
          />
        </Box>
        <Button
          variant="contained"
          type="submit"
          color="primary"
          disableElevation
        >
          Register
        </Button>

        <Typography>
          Already have an account <Link to="/signin">Sign In</Link>
        </Typography>
      </form>
    </div>
  );
};

export default Profile;
