import { Box, makeStyles } from "@material-ui/core";


const useStyles = makeStyles({
  signUpBackground: {
    backgroundImage: `url('https://app.365dropship.com/catalog_bg.3808158baad738767615.jpg')`,
    width: "100%",
    height: "100vh",
    backgroundPosition: "center",
    backgroundSize: "cover",
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
  },
  signUpInput: {
    width: "420px",
    backgroundColor: "#fff",
    borderRadius: "10px",
  },
});

const Authorization = ({ component }) => {
  const classes = useStyles();

  return (
    <Box className={classes.signUpBackground}>
      <Box className={classes.signUpInput} p={5}>
        {component}
      </Box>
    </Box>
  );
};

export default Authorization;
