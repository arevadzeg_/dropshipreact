import * as yup from "yup";

export const signUpValidationSchema = yup.object().shape({
  name: yup.string().required("required"),
  lastName: yup.string().required("required"),
  email: yup.string().email("must be a valied email").required("required"),
  password: yup.string().required("required").min(3),
  passwordConformation: yup
    .string()
    .required("required")
    .oneOf([yup.ref("password"), null], "Passwords must match"),
});

export const signInValidationSchema = yup.object().shape({
  email: yup.string().email().required(),
  password: yup.string().required(),
});
