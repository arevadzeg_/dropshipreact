import { useLocation } from "react-router";
import CatalogItemWraper from "./CatalogItemWraper";
import { useEffect, useState } from "react";
import CatalogNoResult from "./CatalogNoResult";
import { Grid, Backdrop, makeStyles } from "@material-ui/core";
import Modal from "./Modal";
import { addToCart } from "../API/api";
import { useSnackbar } from "notistack";
import { useSelector } from "react-redux";

import AddEditModal from "./Admin/AddEditModal";


// test Commit

const useStyles = makeStyles({
  backDrop: {
    zIndex: "5",
  },
});

const AllProducts = () => {
  const [backDrop, setBackDrop] = useState(true);
  const arreyToMapGifs = Array.from(Array(10).keys());
  const classes = useStyles();
  const location = useLocation();
  const { enqueueSnackbar } = useSnackbar();

  const addToInventory = (id) => {
    let data = { qty: 1 };
    if (typeof id == "number") {
      data.productId = id;
    } else {
      const queryString = new URLSearchParams(location.search);
      data.productId = queryString.get("id");
    }
    addToCart(data)
      .then(() =>
        enqueueSnackbar("Product Added successfully", {
          variant: "success",
          autoHideDuration: 2000,
        })
      )
      .catch((err) =>
        enqueueSnackbar(err.response.data.message, {
          variant: "error",
          autoHideDuration: 2000,
        })
      );
  };

  const allProducts = useSelector((state) => state.products.allProducts);
  const displayProducts = useSelector(
    (state) => state.products.displayProducts
  );

  useEffect(() => {
    if (allProducts.length > 0) {
      setBackDrop(false);
    }
  }, [allProducts]);

  return (
    <>
      <Grid container>
        <Backdrop open={backDrop} className={classes.backDrop}>
          <img
            src="https://media.giphy.com/media/dhnizdZIAW09XPo5xk/giphy.gif"
            alt="img"
          />
        </Backdrop>
        {displayProducts.length > 0 ? (
          displayProducts.map((item) => {
            return (
              <Grid item xs={12} sm={6} md={6} lg={4} xl={3} key={item.id}>
                <CatalogItemWraper
                  title={item.title}
                  price={item.price}
                  image={item.imageUrl}
                  description={item.description}
                  id={item.id}
                  key={item.id}
                  addToInventory={addToInventory}
                />
              </Grid>
            );
          })
        ) : allProducts.length === 0 ? (
          arreyToMapGifs.map((item) => {
            return (
              <Grid item xs={12} sm={12} md={6} lg={4} xl={3} key={item}>
                <CatalogItemWraper
                  key={item}
                  image={
                    "https://media.giphy.com/media/dqsSQ56HhUaM6lHvXP/giphy.gif"
                  }
                />
              </Grid>
            );
          })
        ) : (
          <div></div>
        )}

        <Modal addToInventory={addToInventory} />
        <AddEditModal />

        {displayProducts.length === 0 && allProducts.length > 0 && (
          <Grid item xs={12}>
            <CatalogNoResult />
          </Grid>
        )}
      </Grid>
    </>
  );
};

export default AllProducts;
