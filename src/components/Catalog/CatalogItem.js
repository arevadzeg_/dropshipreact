import { useHistory } from "react-router";
const CatalogItem = ({ image, title, price, id }) => {
  const history = useHistory();
  const handleOpenModal = (e) => {
    const queryString = new URLSearchParams(history.location.search);
    queryString.set("id", id);
    history.push(`${history.location.pathname}?${queryString.toString()}`);
  };

  return (
    <div className="catalog__item" id={id} onClick={handleOpenModal}>
      <img className="catalog__img" src={image} alt="" />
      <p className="catalog__title">{title}</p>
      <div className="catalog__provider">
        <p>By:</p>
        <p className="catalog__provider-name">SP-Supplier126</p>
      </div>
      <div className="catalog__details">
        <div className="catalog__RRP">
          <p>$52</p>
          <p>RRP</p>
        </div>
        <div className="catalog__price">
          <p>${price}</p>
          <p>COSTS</p>
        </div>
        <div className="catalog__profit">
          <p>12%($6)</p>
          <p>PROFIT</p>
        </div>
      </div>
    </div>
  );
};

export default CatalogItem;
