import { Dialog, CircularProgress, Box, Button } from "@material-ui/core";
import { makeStyles } from "@material-ui/styles";
import { useTheme } from "@material-ui/core/styles";
import "./Modal.css";
import { useDispatch, useSelector } from "react-redux";
import useMediaQuery from "@material-ui/core/useMediaQuery";
import { useHistory } from "react-router";
import { closeModal } from "../Redux/ProductsReducer/productsActions";

const useStyles = makeStyles({
  root: {
    zIndex: "300000 !important",
  },
  cart_buttons: {
    display: "flex",
    justifyContent: "space-between",
  },
  cart_button: {
    flexGrow: "0.4",
  },
  add_button: {
    textAlign: "right",
  },
});

const Modal = ({ removeProduct, addToInventory }) => {
  const theme = useTheme();
  const classes = useStyles();
  const fullScreen = useMediaQuery(theme.breakpoints.down("sm"));
  const modal = useSelector((state) => state.products.modal);
  const history = useHistory();
  const dispatch = useDispatch();

  const handleCloseModal = () => {
    dispatch(closeModal());
    const queryString = new URLSearchParams(history.location.search);
    queryString.delete("id");
    history.push(`${history.location.pathname}?${queryString.toString()}`);
  };

  return (
    <Dialog
      open={modal ? true : false}
      onClose={handleCloseModal}
      className={classes.root}
      maxWidth="lg"
      fullScreen={fullScreen}
    >
      {modal && (
        <div className="modal">
          <div className="modal__left">
            <div className="modal__left-header">
              <div>
                <p>$20</p>
                <p>RRP</p>
              </div>
              <div>
                <p>${modal.price}</p>
                <p>COST</p>
              </div>
              <div className="modal__profit">
                <p>20%($6)</p>
                <p>PROFIT</p>
              </div>
            </div>
            <div className="modal__image">
              {modal.imageUrl ? (
                <img src={modal.imageUrl} alt="" />
              ) : (
                <Box mt={15}>
                  <CircularProgress size={70} />
                </Box>
              )}
            </div>
          </div>

          <div className="modal__right">
            <div className="modal__supplier">
              <p>SKU# bgb-s0724578 COPY</p>
              <p className="modal__supplier-details">
                <span>Supplier:</span> <span>SP-Supplier115</span>
              </p>
            </div>
            <p className="modal__title">{modal.title}</p>
            {removeProduct ? (
              <Box className={classes.cart_buttons}>
                <Button
                  variant="contained"
                  color="primary"
                  disableElevation
                  size="large"
                  className={classes.cart_button}
                >
                  Buy It Now
                </Button>
                <Button
                  variant="outlined"
                  disableElevation
                  size="large"
                  className={classes.cart_button}
                  onClick={removeProduct}
                >
                  Remove from Cart
                </Button>
              </Box>
            ) : (
              <Box className={classes.add_button}>
                <Button
                  variant="contained"
                  color="primary"
                  disableElevation
                  size="large"
                  className="modal__add"
                  onClick={addToInventory}
                >
                  Add to Inventory
                </Button>
              </Box>
            )}
            <div className="modal__details">
              <div className="modal__details-header">
                <div className="modal__details--active">Product Details</div>
                <div>Shopping Rates</div>
              </div>
              <div className="modal__description">
                <p>{modal.description}</p>
              </div>
            </div>
          </div>

          <i className="fas fa-times" onClick={handleCloseModal}></i>
        </div>
      )}
    </Dialog>
  );
};

export default Modal;
