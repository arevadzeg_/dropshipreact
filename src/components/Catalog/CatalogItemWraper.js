import "./CatalogItem.css";
import AddToInventoryBtn from "./AddToInventoryBtn";
import ItemCheckBox from "./ItemCheckBox";
import CatalogItem from "./CatalogItem";
import { useSelector } from "react-redux";
import ModifyProduct from "./Admin/ModifyProduct";

export default function CatalogItemWraper({
  image,
  title,
  price,
  description,
  id,
  addToInventory,
}) {
  const selectedProducts = useSelector(
    (state) => state.products.selectedProducts
  );

  const isAdmin = JSON.parse(localStorage.getItem("isAdmin"));

  return (
    <div
      className={`catalog__item-wraper ${
        selectedProducts.includes(id) ? "border_visible" : ""
      }`}
      id={id}
    >
      <ItemCheckBox id={id} />
      <AddToInventoryBtn id={id} addToInventory={() => addToInventory(id)} />
      {isAdmin && (
        <ModifyProduct
          id={id}
          title={title}
          price={price}
          image={image}
          description={description}
        />
      )}
      <CatalogItem image={image} title={title} price={price} id={id} />
    </div>
  );
}
