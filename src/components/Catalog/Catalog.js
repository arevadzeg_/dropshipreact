import "./Catalog.css";
import { useHistory } from "react-router-dom";
import AllProducts from "./AllProducts";
import { useEffect } from "react";
import { checkTokenValidity } from "../API/api";

export default function Catalog() {
  const history = useHistory();
  useEffect(() => {
    checkTokenValidity()
      .then()
      .catch((s) => {
        history.push("/signin");
      });
  }, [history]);

  return (
    <div className="catalog">
      <AllProducts />
    </div>
  );
}
