import "./ItemCheckBox.css";
import CheckCircleIcon from "@material-ui/icons/CheckCircle";
import CheckCircleOutlineRoundedIcon from "@material-ui/icons/CheckCircleOutlineRounded";
import { useDispatch, useSelector } from "react-redux";
import { selectProduct } from "../Redux/ProductsReducer/productsActions";

import Checkbox from "@material-ui/core/Checkbox";

export default function ItemCheckBox({ id, shown }) {
  const dispatch = useDispatch();
  const selectedProducts = useSelector(
    (state) => state.products.selectedProducts
  );

  const HandleSelectProduct = () => {
    let selectedClone = selectedProducts.slice();
    if (!selectedClone.includes(id)) {
      selectedClone.push(id);
    } else if (selectedClone.includes(id)) {
      selectedClone = selectedClone.filter((i) => i !== id);
    }
    dispatch(selectProduct(selectedClone));
  };

  return (
    <div
      className={`catalog__checkbox ${
        selectedProducts.includes(id) || shown ? "displayBlock" : ""
      }`}
    >
      <Checkbox
        inputProps={{ "aria-label": "primary checkbox" }}
        onChange={HandleSelectProduct}
        checked={selectedProducts.includes(id)}
        id={String(id)}
        checkedIcon={<CheckCircleIcon />}
        icon={<CheckCircleOutlineRoundedIcon />}
        style={{ color: "#61D5DF" }}
      />
    </div>
  );
}
