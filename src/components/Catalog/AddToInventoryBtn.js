import "./AddToInventoryBtn.css";
import { useSelector } from "react-redux";

const AddToInventoryBtn = ({ id, addToInventory }) => {
  const selectedProducts = useSelector(
    (state) => state.products.selectedProducts
  );
  return (
    <p
      className={`catalog__add  ${
        selectedProducts.includes(id) ? "displayBlock" : ""
      }`}
      onClick={addToInventory}
      id={id}
    >
      Add to Inventory
    </p>
  );
};

export default AddToInventoryBtn;
