const CatalogNoResult = () => {
  return (
    <div
      className="catalog-no-result"
      style={{
        backgroundImage:
          "url(https://cdn.dribbble.com/users/1554526/screenshots/3399669/no_results_found.png)",
      }}
    ></div>
  );
};

export default CatalogNoResult;
