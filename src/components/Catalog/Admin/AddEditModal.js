import {
  Box,
  TextField,
  Typography,
  Button,
  Backdrop,
  ButtonGroup,
  Modal,
  Fade,
} from "@material-ui/core";
import AddIcon from "@material-ui/icons/Add";
import { Formik, Form } from "formik";
import DeleteIcon from "@material-ui/icons/Delete";
import { addPorduct, editProduct } from "../../API/api";
import EditIcon from "@material-ui/icons/Edit";
import { useSnackbar } from "notistack";
import validationSchema from "./validation";
import useStyles from "./adminStyles";
import { useDispatch, useSelector } from "react-redux";
import { closeAdminModal } from "../../Redux/AdminReducer/adminActions";
import {
  fillUpDisplay,
  filterDisplay,
} from "../../Redux/ProductsReducer/productsActions";

const AddEditModal = ({ handleDeleteProduct }) => {
  const allProducts = useSelector((state) => state.products.allProducts);
  const dispatch = useDispatch();

  const handleAddProducts = (data) => {
    addPorduct(data)
      .then((s) => {
        enqueueSnackbar("Successfully Added", { variant: "success" });
        allProducts.push(s.data.data);
        dispatch(fillUpDisplay(allProducts));
        dispatch(filterDisplay(allProducts));
      })
      .catch((err) =>
        enqueueSnackbar(
          err.response.data.message ? err.response.data.message : "Error",
          { variant: "error" }
        )
      );
  };

  const handleCloseEdit = () => {
    dispatch(closeAdminModal());
  };

  const openEdit = useSelector((state) => state.admin.modalOpen);
  const editMode = typeof openEdit === "object" ? true : false;

  const classes = useStyles();

  const { enqueueSnackbar } = useSnackbar();

  return (
    <Modal
      className={classes.modal}
      open={openEdit ? true : false}
      onClose={handleCloseEdit}
      closeAfterTransition
      BackdropComponent={Backdrop}
    >
      <Fade in={openEdit ? true : false}>
        <Box>
          <Formik
            enableReinitialize
            initialValues={
              editMode
                ? {
                    title: openEdit.title,
                    description: openEdit.description,
                    price: openEdit.price,
                    imageUrl: openEdit.image,
                  }
                : {
                    title: "",
                    description: "",
                    price: "",
                    imageUrl: "",
                  }
            }
            onSubmit={(values) => {
              editMode
                ? editProduct(openEdit.id, values)
                    .then((s) => {
                      const z = allProducts.filter(
                        (item) => item.id !== openEdit.id
                      );
                      z.push(s.data.data);
                      dispatch(fillUpDisplay(z));

                      enqueueSnackbar("Successfully Edited", {
                        variant: "success",
                      });
                    })
                    .catch((err) =>
                      enqueueSnackbar(err.response.data.message, {
                        variant: "error",
                      })
                    )
                : handleAddProducts(values);
              handleCloseEdit();
            }}
            validationSchema={validationSchema}
          >
            {({ values, errors, handleChange, handleBlur, touched }) => {
              return (
                <Form className={classes.form}>
                  <Typography variant="overline"> Welcome Admin</Typography>
                  <Typography variant="overline">
                    {editMode ? "Edit " : "Add "} Products
                  </Typography>
                  <TextField
                    label="Title"
                    name="title"
                    onChange={handleChange}
                    helperText={touched.title && errors.title}
                    error={touched.title && errors.title && true}
                    onBlur={handleBlur}
                    value={values.title}
                  ></TextField>
                  <TextField
                    label="Description"
                    name="description"
                    onChange={handleChange}
                    helperText={touched.description && errors.description}
                    error={touched.description && errors.description && true}
                    onBlur={handleBlur}
                    value={values.description}
                  ></TextField>
                  <TextField
                    label="Price"
                    name="price"
                    onChange={handleChange}
                    helperText={touched.price && errors.price}
                    error={touched.price && errors.price && true}
                    onBlur={handleBlur}
                    value={values.price}
                  ></TextField>
                  <TextField
                    label="Image"
                    name="imageUrl"
                    onChange={handleChange}
                    helperText={touched.imageUrl && errors.imageUrl}
                    error={touched.imageUrl && errors.imageUrl && true}
                    onBlur={handleBlur}
                    value={values.imageUrl}
                  ></TextField>
                  <ButtonGroup>
                    <Button
                      startIcon={openEdit ? <EditIcon /> : <AddIcon />}
                      variant="contained"
                      color="secondary"
                      disableElevation
                      type="submit"
                    >
                      {editMode ? "Edit" : "Add"}
                    </Button>
                    {editMode && (
                      <Button
                        endIcon={<DeleteIcon />}
                        color="secondary"
                        variant="outlined"
                        disableElevation
                        onClick={handleDeleteProduct}
                      >
                        Delete
                      </Button>
                    )}
                  </ButtonGroup>
                </Form>
              );
            }}
          </Formik>
        </Box>
      </Fade>
    </Modal>
  );
};

export default AddEditModal;
