import * as yup from "yup";

const validationSchema = yup.object().shape({
  title: yup.string().required().min(3, "Too Short").max(15, "Too Long"),
  description: yup
    .string()
    .required()
    .min(10, "Too Short")
    .max(255, "Too Long"),
  price: yup
    .number()
    .typeError("Must be a nubmer ")
    .required()
    .min(1, "Too Cheap Price")
    .max(10000, "Too Expansie"),
  imageUrl: yup.string().url().required(),
});

export default validationSchema;
