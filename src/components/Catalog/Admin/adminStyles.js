import { makeStyles } from "@material-ui/styles";

const useStyles = makeStyles({
  editDelete: {
    position: "absolute",
  },
  paper: { background: "#fff" },
  modal: {
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
  },
  form: {
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
    justifyContent: "center",
    gap: "20px",
    backgroundColor: "#fff",
    width: "500px",
    padding: "30px",
    borderRadius: "10px",
  },
});

export default useStyles;
