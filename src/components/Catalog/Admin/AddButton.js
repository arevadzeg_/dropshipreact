import { Button } from "@material-ui/core";
import AddBoxIcon from "@material-ui/icons/AddBox";
import { useDispatch } from "react-redux";
import { openAdminModal } from "../../Redux/AdminReducer/adminActions";

const AddButton = () => {
  const dispatch = useDispatch();
  const openModal = () => {
    dispatch(openAdminModal(true));
  };

  return (
    <Button
      color="primary"
      disableElevation
      startIcon={<AddBoxIcon />}
      onClick={openModal}
    >
      Add new product
    </Button>
  );
};

export default AddButton;
