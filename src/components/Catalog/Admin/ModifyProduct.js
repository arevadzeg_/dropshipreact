import { useState } from "react";
import IconButton from "@material-ui/core/IconButton";
import Menu from "@material-ui/core/Menu";
import MenuItem from "@material-ui/core/MenuItem";
import MoreVertIcon from "@material-ui/icons/MoreVert";
import { deleteProduct } from "../../API/api";
import { useSnackbar } from "notistack";
import { useDispatch, useSelector } from "react-redux";
import {
  fillUpDisplay,
  filterDisplay,
} from "../../Redux/ProductsReducer/productsActions";

import useStyles from "./adminStyles";
import { openAdminModal } from "../../Redux/AdminReducer/adminActions";

export default function ModifyProduct({
  id,
  title,
  price,
  image,
  description,
}) {
  const [anchorEl, setAnchorEl] = useState(null);
  const open = Boolean(anchorEl);
  const handleClick = (event) => {
    setAnchorEl(event.currentTarget);
  };
  const handleClose = () => {
    setAnchorEl(null);
  };

  const { enqueueSnackbar } = useSnackbar();
  const dispatch = useDispatch();
  const displayProducts = useSelector(
    (state) => state.products.displayProducts
  );

  const handleDeleteProduct = () => {
    deleteProduct(id)
      .then(() =>
        enqueueSnackbar("Successfully Deleted", { variant: "success" })
      )
      .catch((err) =>
        enqueueSnackbar(
          err.response.data.message
            ? err.response.data.message
            : "something went wrong",
          { variant: "error" }
        )
      );
    const fileteredCart = displayProducts.filter((item) => item.id !== id);

    dispatch(fillUpDisplay(fileteredCart));
    dispatch(filterDisplay(fileteredCart));
  };

  const classes = useStyles();

  const handleOpenEdit = () => {
    const data = { title, price, image, description, id };
    dispatch(openAdminModal(data));

    setAnchorEl(null);
  };

  return (
    <>
      <div className={classes.editDelete}>
        <IconButton onClick={handleClick}>
          <MoreVertIcon />
        </IconButton>
        <Menu anchorEl={anchorEl} keepMounted open={open} onClose={handleClose}>
          <MenuItem onClick={handleOpenEdit}>Edit</MenuItem>
          <MenuItem onClick={handleDeleteProduct}>Delete</MenuItem>
        </Menu>
      </div>
    </>
  );
}
