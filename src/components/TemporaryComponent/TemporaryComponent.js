import { Button, makeStyles } from "@material-ui/core";
import { useHistory } from "react-router-dom";

const TemporaryComponent = ({ where }) => {
  const useStyles = makeStyles({
    centerContent: {
      display: "flex",
      justifyContent: "center",
      alignItems: "center",
      flexDirection: "column",
    },
    img: {
      backgroundRepeat: "no-repeat",
      backgroundImage:
        "url(https://media.istockphoto.com/videos/sign-under-construction-video-id1239504264?s=640x640)",
      width: "250px",
      height: "350px",
      backgroundPositionX: "center",
    },
  });
  const history = useHistory();
  const handleBack = () => {
    history.goBack();
  };

  const classes = useStyles();

  return (
    <div
      style={{ width: "calc(100% - 55px)" }}
      className={classes.centerContent}
    >
      <div className={classes.img}></div>
      <Button onClick={handleBack} color="primary" variant="contained">
        BACK
      </Button>
    </div>
  );
};

export default TemporaryComponent;
