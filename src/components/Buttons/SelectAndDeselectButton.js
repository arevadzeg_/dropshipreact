import { Hidden, Button } from "@material-ui/core";
import { useSelector, useDispatch } from "react-redux";
import { selectAndDeselectAll } from "../Redux/ProductsReducer/productsActions";
import useStyles from "../Header/searchStyles";

const SelectAndDeselectButton = () => {
  const dispatch = useDispatch();
  const displayProducts = useSelector(
    (state) => state.products.displayProducts
  );
  const selectedProducts = useSelector(
    (state) => state.products.selectedProducts
  );

  const handleSelectAndDeselectAll = () => {
    const selected = [];
    if (selectedProducts.length !== displayProducts.length) {
      displayProducts.map((product) => selected.push(product.id));
    }
    dispatch(selectAndDeselectAll(selected));
  };

  const classes = useStyles();

  return (
    <Hidden lgUp>
      <Button
        variant="contained"
        color="primary"
        disableElevation
        className={classes.selectAllBtn}
        onClick={handleSelectAndDeselectAll}
      >
        <i className="fas fa-check-circle"></i>
      </Button>
    </Hidden>
  );
};

export default SelectAndDeselectButton;
