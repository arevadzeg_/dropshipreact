import { Button, Hidden } from "@material-ui/core";
import { useDispatch, useSelector } from "react-redux";
import { selectAll } from "../Redux/ProductsReducer/productsActions";

const SelectAllBtn = () => {
  const dispatch = useDispatch();

  const displayProducts = useSelector(
    (state) => state.products.displayProducts
  );
  const handleSelectAll = () => {
    const allSelected = [];
    displayProducts.filter((item) => allSelected.push(item.id));
    dispatch(selectAll(allSelected));
  };

  return (
    <Hidden mdDown>
      <Button
        variant="contained"
        color="primary"
        disableElevation
        onClick={handleSelectAll}
      >
        SELECT ALL
      </Button>
    </Hidden>
  );
};

export default SelectAllBtn;
