import sort from "./sortingWays.js";

const filteringWays = (options, array) => {
  let finalResult = array.slice();

  if (finalResult && options) {
    if (options.searchQuery) {
      finalResult = finalResult.filter((item) => {
        return item.title
          .toLowerCase()
          .includes(options.searchQuery.toLowerCase());
      });
    }

    if (options.sort) {
      sort(options.sort, finalResult);
    }
  }
  return finalResult;
};

export default filteringWays;
