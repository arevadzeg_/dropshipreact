import axios from "axios";

const SERVER_URL = "http://18.185.148.165:3000/";
const SERVER_URL_V1 = SERVER_URL + "api/v1/";

axios.interceptors.request.use((config) => {
  config.headers = {
    Authorization: `Bearer ${localStorage.getItem("token")}`,
  };
  return config;
});

axios.interceptors.response.use(
  (response) => {
    return response;
  },
  (error) => {
    if (error.response.status === 401 || error.response.status === 403) {
      if (
        window.location.pathname === "/signin" ||
        window.location.pathname === "/signup"
      ) {
      } else {
        console.log(error);
        // window.location = "./signin";
      }
    }
    console.log(error);
    throw error;
  }
);

export const handleRegistration = async (data) => {
  try {
    const responce = await axios.post(SERVER_URL + "register", data);
    localStorage.setItem("user", JSON.stringify(responce.data.data));
    localStorage.setItem("token", responce.data.data.token);
    localStorage.setItem("isAdmin", responce.data.data.isAdmin ? true : false);
    window.location = "/catalog";
    return responce;
  } catch (err) {
    console.log(err);
  }
};

export const handleLogIn = async (data) => {
  try {
    const responce = await axios.post(SERVER_URL + "login", data);
    localStorage.setItem("user", JSON.stringify(responce.data.data));
    localStorage.setItem("token", responce.data.data.token);
    localStorage.setItem("isAdmin", responce.data.data.isAdmin ? true : false);

    window.location.href = "/catalog";
    return responce;
  } catch (err) {
    throw err;
  }
};

export const checkTokenValidity = async () => {
  try {
    const responce = await axios.get(SERVER_URL_V1 + "cart");
    return responce;
  } catch (err) {
    console.log(err.response.status);
  }
};

export const getUserCart = async () => {
  try {
    const response = await axios.get(SERVER_URL_V1 + "cart");
    return response;
  } catch (err) {
    console.log(err);
  }
};

export const addToCart = async (data) => {
  try {
    const response = await axios.post(SERVER_URL_V1 + "cart/add", data);
    return response;
  } catch (err) {
    throw err;
  }
};

export const updateCart = async (id, quantity) => {
  try {
    const response = await axios.post(SERVER_URL_V1 + "/cart/update/" + id, {
      qty: quantity,
    });
    return response;
  } catch (err) {
    console.log(err);
  }
};

export const removeProductFromCart = async (id) => {
  try {
    const response = await axios.post(SERVER_URL_V1 + "cart/remove/" + id);
    return response;
  } catch (err) {
    throw err;
  }
};

export const addPorduct = async (data) => {
  try {
    const response = await axios.post(SERVER_URL_V1 + "products", data);
    return response;
  } catch (err) {
    throw err;
  }
};

export const getSingleProduct = async (id) => {
  try {
    const response = await axios.get(SERVER_URL_V1 + "products/" + id);
    return response;
  } catch (err) {
    throw err;
  }
};

export const getAllProducts = async () => {
  try {
    const response = await axios.get(SERVER_URL_V1 + "products/");
    return response;
  } catch (err) {
    console.log(err);
  }
};

export const editProduct = async (id, data) => {
  try {
    const response = await axios.put(SERVER_URL_V1 + "products/" + id, data);
    return response;
  } catch (err) {
    throw err;
  }
};

export const deleteProduct = async (id) => {
  try {
    const response = await axios.delete(SERVER_URL_V1 + "products/" + id);
    return response;
  } catch (err) {
    throw err;
  }
};

export const handleDataUpdate = async (id, data) => {
  try {
    const response = await axios.put(SERVER_URL_V1 + "users/" + id, data);
    return response;
  } catch (err) {
    throw err;
  }
};
