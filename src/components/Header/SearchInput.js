import "./SearchInput.css";
import { useEffect, useState } from "react";
import { useHistory } from "react-router-dom";
import { Box } from "@material-ui/core";
import { Hidden } from "@material-ui/core";
import { makeStyles } from "@material-ui/styles";
import { debounce } from "debounce";

const useStyles = makeStyles({
  smallSearch: {
    float: "right",
    fontSize: "20px",
    border: "1px solid rgba(95, 102, 115, 0.1)",
    borderRadius: "5px",
    padding: "5px 10px 6px 10px",
    background: "#fff",
  },
});

const SearchInput = () => {
  const [query, setQuery] = useState("");
  const [toggleSearch, setToggleSearch] = useState(false);
  const history = useHistory();

  const classes = useStyles();

  const openSearch = () => {
    setToggleSearch(!toggleSearch);
  };

  const hadleSearch = (e) => {
    e.preventDefault();
    const QueryString = new URLSearchParams(history.location.search);
    if (query) {
      QueryString.set("search", query);
    } else {
      QueryString.delete("search");
    }
    history.push(`${history.location.pathname}?${QueryString.toString()}`);
  };

  const changeQuery = (e) => {
    setQuery(e.target.value);
  };

  //Displays search value in the input field if you open from the link
  useEffect(() => {
    const queryString = new URLSearchParams(history.location.search);
    if (queryString.get("search")) {
      setQuery(queryString.get("search"));
    }
  }, [history.location.search]);

  useEffect(() => {
    const x = debounce(() => {
      const queryString = new URLSearchParams(history.location.search);
      if (query) {
        queryString.set("search", query);
      } else {
        queryString.delete("search");
      }
      history.push(`${history.location.pathname}?${queryString.toString()}`);
    }, 400);
    x();
    return () => {
      x.clear();
    };
  }, [query, history]);

  return (
    <>
      <Box>
        <Hidden smUp>
          <Box className={classes.smallSearch}>
            {toggleSearch === false && (
              <i className="fas fa-search" onClick={openSearch}></i>
            )}

            {toggleSearch && (
              <form
                className="search-sort__search-small"
                onSubmit={hadleSearch}
              >
                <input
                  type="text"
                  placeholder="search..."
                  value={query}
                  onChange={changeQuery}
                ></input>
                <i
                  className="fas fa-times"
                  id="closeSearch"
                  onClick={openSearch}
                ></i>
              </form>
            )}
          </Box>
        </Hidden>
      </Box>
      <Box>
        <Hidden xsDown>
          <form className="search-sort__searchbar" onSubmit={hadleSearch}>
            <input
              type="text"
              placeholder="search..."
              id="search-text"
              onChange={changeQuery}
              value={query}
            />

            <i className="fas fa-search" onClick={hadleSearch}></i>
          </form>
        </Hidden>
      </Box>
    </>
  );
};

export default SearchInput;
