import "./Sort.css";
import { Link, useHistory, useLocation } from "react-router-dom";
import { Box, makeStyles } from "@material-ui/core";

const Sort = ({ AddButton }) => {
  const location = useLocation();
  let history = useHistory();
  const useStyles = makeStyles({
    sortLayout: {
      display: "flex",
      justifyContent: "space-between",
      color: "#9b9fa8",
      padding: "2px 100px 3px 20px",
      height: "39px",
      borderBottom: "1px solid #dbe0f3",
      alignItems: "center",
    },
  });

  const classes = useStyles();
  const isAdmin = JSON.parse(localStorage.getItem("isAdmin"));

  function handleChange(value) {
    const queryString = new URLSearchParams(location.search);
    if (value !== "default") {
      queryString.set("sort", value);
    } else {
      queryString.delete("sort");
    }

    history.push(`${location.pathname}?${queryString}`);
  }

  return (
    <Box className={classes.sortLayout}>
      <div className="search-sort__sort">
        <i className="fas fa-sort-amount-up"></i>

        <select
          className="search-sort__select"
          onChange={(event) => handleChange(event.target.value)}
        >
          <option value="default">Sort By: Default</option>
          <option tag={Link} to="/" value="asc">
            Assending by price
          </option>
          <option value="desc">Dessending by price</option>
          <option value="ascName">Assending by name</option>
          <option value="descName">Dessending by name</option>
        </select>
        <i className="fas fa-sort"></i>
      </div>
      {AddButton && isAdmin && <AddButton />}
    </Box>
  );
};

export default Sort;
