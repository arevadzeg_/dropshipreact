import { makeStyles } from "@material-ui/core/styles";

const useStyles = makeStyles({
  productQuantityText: {
    fontSize: "12px",
    color: "#29304b",
    fontWeight: "600",
  },

  selectAllBtn: {
    height: "35px",
    minWidth: "40px",
    fontSize: "16px",

    "@media (max-width:600px)": {
      height: "40px",
      minWidth: "40px",
      fontSize: "18px",
    },
  },

  addSmallBtn: {
    "@media (max-width:600px)": {
      height: "40px",
      minWidth: "40px",
    },
  },
  flexBox: {
    display: "flex",
  },

  flexRightBox: {
    flex: "3",
    gap: "10px",
    "@media (max-width:1278px)": {
      flex: "1.3",
    },
    "@media (max-width:850px)": {
      flex: "3",
    },
  },

  flexLeftBox: {
    flex: "3",
    justifyContent: "flex-end",
    display: "flex",
    gap: "10px",
    "@media (max-width:1070px)": {
      marginRight: "60px",
    },
  },
  searchInput: {
    flexGrow: "1",
    maxWidth: "800px",
  },
});

export default useStyles;
