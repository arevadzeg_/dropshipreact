import { makeStyles } from "@material-ui/core/styles";
export const useStyles = makeStyles({
  paper: {
    width: 200,
  },
  logo: {
    paddingTop: "10px",
    paddingBottom: "15px",
    marginRight: "10px",
    borderBottom: "1px solid #DDE3EE",
  },

  text: {
    fontSize: "8px",
    color: "#677791",
  },
  icon: {
    minWidth: "46px",
    fonstSize: "28px",
    color: "#49547d",
    "&:hover": {
      color: "rgb(97, 213, 223)",
    },
  },
  position: {
    cursor: "pointer",
    marginTop: "5px",
  },
});

export const drawerItems = [
  {
    src: "https://app.365dropship.com/assets/images/profile-example.jpg",
    link: "/profile",
    text: "Profile",
  },
  {
    icon: "fa-dice-d20",
    link: "/dashboard",
    text: "Dashboard",
  },
  {
    icon: "fa-address-card",
    link: "/catalog",
    text: "Catalog",
  },
  {
    icon: "fa-dice-d6",
    link: "/inventory",
    text: "Inventory",
  },
  {
    icon: "fa-shopping-cart",
    link: "/cart",
    text: "Cart",
  },
  {
    icon: "fa-clipboard-check",
    link: "/orders",
    text: "Orders",
  },
  {
    icon: "fa-clipboard-list",
    link: "/transactions",
    text: "Transactions",
  },
  {
    icon: "fa-exchange-alt",
    link: "/store",
    text: "Stores List",
  },
];
