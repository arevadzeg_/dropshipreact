import "./BurgerMenu.css";
import { useHistory } from "react-router";
import {
  Drawer,
  List,
  ListItem,
  ListItemIcon,
  ListItemText,
} from "@material-ui/core";
import { useStyles, drawerItems } from "./BurgerDrawerStyles";

const BurgerMenu = ({ drawerState, handleDrawerOpen }) => {
  const classes = useStyles();
  const history = useHistory();

  return (
    <>
      <Drawer
        anchor="right"
        open={drawerState}
        onClose={handleDrawerOpen}
        className="Drawer"
        classes={{ paper: classes.paper }}
      >
        <List>
          <ListItem className={classes.logo}>
            <ListItemIcon>
              <img
                src="https://app.365dropship.com/assets/images/dropship_logo.png"
                style={{ width: "60px", cursor: "pointer" }}
                alt=""
                onClick={() => history.push("/")}
              />
            </ListItemIcon>
          </ListItem>
          {drawerItems.map((item) => {
            return (
              <ListItem
                key={item.text}
                className={classes.position}
                onClick={() => history.push(`${item.link}`)}
              >
                {item.src ? (
                  <>
                    <ListItemText>{item.text}</ListItemText>
                    <ListItemIcon>
                      <img
                        alt=""
                        src={item.src}
                        style={{
                          width: "40px",
                          borderRadius: "50% ",
                          height: "40px",
                          padding: "2px",
                          border: "1px solid #61D5DF",
                        }}
                      />
                    </ListItemIcon>
                  </>
                ) : (
                  <>
                    <ListItemText className={classes.text}>
                      {item.text}
                    </ListItemText>
                    <ListItemIcon className={classes.icon}>
                      <i className={`fas ${item.icon}`}></i>
                    </ListItemIcon>
                  </>
                )}
              </ListItem>
            );
          })}
        </List>
      </Drawer>
    </>
  );
};

export default BurgerMenu;
