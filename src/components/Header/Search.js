import SearchInput from "./SearchInput";
import HelpButton from "./HelpButton";
import FilterDrawer from "../Filter/FilterDrawer";
import { Hidden, Button, Box, Typography } from "@material-ui/core";
import classNames from "classnames";
import useStyles from "./searchStyles";
import { addToCart } from "../API/api";
import { useSnackbar } from "notistack";
import { useDispatch, useSelector } from "react-redux";
import { deselectProducts } from "../Redux/ProductsReducer/productsActions";
import SelectAndDeselectButton from "../Buttons/SelectAndDeselectButton";
import SelectAllBtn from "../Buttons/SelectAllBtn";

const Search = ({ handleDrawerOpen, drawerState, breakPoint }) => {
  const classes = useStyles();
  const { enqueueSnackbar } = useSnackbar();
  const dispatch = useDispatch();
  const displayProducts = useSelector(
    (state) => state.products.displayProducts
  );
  const selectedProducts = useSelector(
    (state) => state.products.selectedProducts
  );

  const handleDelesectAll = () => {
    dispatch(deselectProducts());
  };

  const addselectedItems = async () => {
    if (selectedProducts.length > 0) {
      enqueueSnackbar("Added to Inventory", { variant: "success" });
      dispatch(deselectProducts());
      for (let i of selectedProducts) {
        const data = { productId: i, qty: 1 };
        await addToCart(data);
      }
    }
  };

  return (
    <Box pl={{ xs: "20px", md: "40px" }} className="search-sort__search">
      <Box className={classNames(classes.flexBox, classes.flexRightBox)}>
        <FilterDrawer breakPoint={breakPoint} />
        <SelectAllBtn />

        <SelectAndDeselectButton />

        <Hidden xsDown>
          <Box className="search-sort__selected-number" pl="20px" pt="2px">
            <Hidden mdDown>
              <Typography
                component="span"
                className={classes.productQuantityText}
              >
                selected {selectedProducts.length} out of{" "}
              </Typography>
            </Hidden>

            <Typography
              component="span"
              className={classes.productQuantityText}
            >
              {displayProducts ? displayProducts.length : 0} products
            </Typography>
          </Box>
        </Hidden>

        <Hidden mdDown>
          {selectedProducts.length > 0 && (
            <Button
              color="primary"
              variant="contained"
              disableElevation
              onClick={handleDelesectAll}
            >
              Clear
            </Button>
          )}
        </Hidden>
      </Box>
      <Box className={classNames(classes.flexBox, classes.flexLeftBox)}>
        <Box className={classes.searchInput}>
          <SearchInput />
        </Box>

        <Hidden smDown>
          <Button
            color="primary"
            disableElevation
            variant="contained"
            onClick={addselectedItems}
          >
            ADD TO INVENTORY
          </Button>
        </Hidden>
        <Hidden mdUp>
          <Button
            color="primary"
            disableElevation
            variant="contained"
            className={classes.addSmallBtn}
            onClick={addselectedItems}
          >
            ADD
          </Button>
        </Hidden>
        <Hidden mdDown>
          <HelpButton />
        </Hidden>
      </Box>
    </Box>
  );
};

export default Search;
