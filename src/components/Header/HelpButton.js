import "./HelpButton.css";

const HelpButton = () => {
  return (
    <div className="search-sort__help">
      <i className="far fa-question-circle"></i>
    </div>
  );
};

export default HelpButton;
