import Sort from "./Sort";
import Search from "./Search";
import "./Header.css";
import AddButton from "../Catalog/Admin/AddButton";

const Header = ({ handleDrawerOpen, drawerState, breakPoint }) => {
  return (
    <div className="search-sort">
      <Search
        handleDrawerOpen={handleDrawerOpen}
        drawerState={drawerState}
        breakPoint={breakPoint}
      />
      <Sort AddButton={AddButton} />
    </div>
  );
};

export default Header;
