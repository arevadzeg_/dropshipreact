const BurgerM = ({ handleDrawerOpen, drawerState }) => {
  return (
    <div
      id="burgerMenu"
      className={`${drawerState ? "open" : ""}`}
      onClick={handleDrawerOpen}
    >
      <span></span>
      <span></span>
      <span></span>
      <span></span>
    </div>
  );
};

export default BurgerM;
