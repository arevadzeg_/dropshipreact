import {
  FILL_UP_ALLPRODUCTS,
  SELECT_AND_DESELECT_PRODUCT,
  DESELECT_ALL,
  SELECT_ALL,
  SELECT_AND_DESELECT_ALL,
  OPEN_MODAL,
  CLOSE_MODAL,
  FILTER_DISPLAY,
  RESET_STORE,
  CHANGE_FILTERS,
  CHANGE_SLIDER_VALUES,
  CHANGE_SLIDER_MIN_MAX,
  CHANGE_DISPLAY,
} from "../Constants/constants";

export const fillUpDisplay = (data) => {
  return {
    type: FILL_UP_ALLPRODUCTS,
    payload: data,
  };
};

export const filterDisplay = (data) => {
  return {
    type: FILTER_DISPLAY,
    payload: data,
  };
};
export const changeDisplay = (data) => {
  return {
    type: CHANGE_DISPLAY,
    payload: data,
  };
};

export const resetStore = () => {
  return {
    type: RESET_STORE,
  };
};

export const changeFilters = (data) => {
  return {
    type: CHANGE_FILTERS,
    payload: data,
  };
};

export const openModal = (data) => {
  return {
    type: OPEN_MODAL,
    payload: data,
  };
};

export const closeModal = () => {
  return {
    type: CLOSE_MODAL,
  };
};

export const selectProduct = (data) => {
  return {
    type: SELECT_AND_DESELECT_PRODUCT,
    payload: data,
  };
};
export const deselectProducts = () => {
  return {
    type: DESELECT_ALL,
  };
};
export const selectAll = (data) => {
  return {
    type: SELECT_ALL,
    payload: data,
  };
};
export const selectAndDeselectAll = (data) => {
  return {
    type: SELECT_AND_DESELECT_ALL,
    payload: data,
  };
};

export const changeSliderMinMax = (data) => {
  return {
    type: CHANGE_SLIDER_MIN_MAX,
    payload: data,
  };
};

export const changeSliderValues = (data) => {
  return {
    type: CHANGE_SLIDER_VALUES,
    payload: data,
  };
};
