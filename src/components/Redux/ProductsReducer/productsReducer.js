import {
  FILL_UP_ALLPRODUCTS,
  SELECT_AND_DESELECT_PRODUCT,
  DESELECT_ALL,
  SELECT_ALL,
  SELECT_AND_DESELECT_ALL,
  OPEN_MODAL,
  CLOSE_MODAL,
  FILTER_DISPLAY,
  RESET_STORE,
  CHANGE_FILTERS,
  CHANGE_SLIDER_VALUES,
  CHANGE_SLIDER_MIN_MAX,
  CHANGE_DISPLAY,
} from "../Constants/constants";
import filteringWays from "../../filteringWays";

const initalState = {
  allProducts: [],
  displayProducts: [],
  filters: {},
  selectedProducts: [],
  modal: false,
  sliderMinMax: [0, 0],
  sliderValues: [0, 0],
};

const productsReducer = (state = initalState, action) => {
  switch (action.type) {
    case FILL_UP_ALLPRODUCTS:
      return { ...state, allProducts: action.payload };
    case RESET_STORE:
      return {
        ...state,
        allProducts: [],
        displayProducts: [],
        filters: {},
        selectedProducts: [],
        modal: false,
        sliderMinMax: [0, 0],
        sliderValues: [0, 0],
      };
    case FILTER_DISPLAY:
      return {
        ...state,
        displayProducts: filteringWays(action.payload, state.allProducts),
      };
    case CHANGE_DISPLAY:
      return {
        ...state,
        displayProducts: action.payload,
      };
    case OPEN_MODAL:
      return { ...state, modal: action.payload };
    case CLOSE_MODAL:
      return { ...state, modal: false };

    case SELECT_AND_DESELECT_PRODUCT:
      return { ...state, selectedProducts: action.payload };

    case DESELECT_ALL:
      return { ...state, selectedProducts: [] };

    case SELECT_ALL:
      return { ...state, selectedProducts: action.payload };

    case SELECT_AND_DESELECT_ALL:
      return { ...state, selectedProducts: action.payload };

    case CHANGE_FILTERS:
      return {
        ...state,
        filters: action.payload,
      };
    case CHANGE_SLIDER_VALUES:
      return {
        ...state,
        sliderValues: action.payload,
      };
    case CHANGE_SLIDER_MIN_MAX:
      return {
        ...state,
        sliderMinMax: action.payload,
      };
    default:
      return state;
  }
};

export default productsReducer;
