import { CLOSE_ADMIN_MODAL, OPEN_ADMIN_MODAL } from "../Constants/constants";

const initalState = {
  modalOpen: false,
};

const adminReducer = (state = initalState, action) => {
  switch (action.type) {
    case OPEN_ADMIN_MODAL:
      return { ...state, modalOpen: action.payload };
    case CLOSE_ADMIN_MODAL:
      return { ...state, modalOpen: false };
    default:
      return state;
  }
};

export default adminReducer;
