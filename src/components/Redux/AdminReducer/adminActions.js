import { CLOSE_ADMIN_MODAL, OPEN_ADMIN_MODAL } from "../Constants/constants";

export const openAdminModal = (data) => {
  return { type: OPEN_ADMIN_MODAL, payload: data };
};
export const closeAdminModal = () => {
  return { type: CLOSE_ADMIN_MODAL };
};
