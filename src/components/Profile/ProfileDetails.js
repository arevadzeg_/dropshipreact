import { Box, Button, Hidden } from "@material-ui/core";
import { useFormik } from "formik";
import { handleDataUpdate } from "../API/api";
import { useState } from "react";
import { changePasswordValidation } from "./changePasswordValidation";
import MenuIcon from "@material-ui/icons/Menu";
import useStyles from "./ProfileStyles";
import ChangeDetailsModal from "./ChangeDetailsModal";
import ChangePasswordModal from "./ChangePasswordModal";
import PersonalDetailsInput from "./PersonalDetailsInput";
import { useSnackbar } from "notistack";

const ProfileDetails = () => {
  const classes = useStyles();
  const [changeDetailsModal, setChangeDetailsModal] = useState(false);
  const [changePasswordModal, setChangePasswordModal] = useState(false);

  const { enqueueSnackbar } = useSnackbar();

  const useData = JSON.parse(localStorage.getItem("user"));

  const formik = useFormik({
    initialValues: {
      firstName: useData.firstName,
      lastName: useData.lastName,
      email: useData.email,
      password: "",
    },
    onSubmit: () => {
      const data = {
        firstName: formik.values.firstName,
        lastName: formik.values.lastName,
        email: formik.values.email,
        password: formik.values.password,
      };
      if (data.password === localStorage.getItem("password")) {
        handleDataUpdate(useData.id, data)
          .then((res) => {
            if (res.status === 200) {
              enqueueSnackbar("Successfully changed details", {
                variant: "success",
              });
              localStorage.setItem("user", JSON.stringify(res.data.data));
              setChangeDetailsModal(false);
              formik.values.password = "";
            }
          })
          .catch((err) => {
            enqueueSnackbar(
              err.response.data.message
                ? err.response.data.message
                : "Failed to change details",
              {
                variant: "error",
              }
            );
          });
      }
    },
  });

  const changePassFormik = useFormik({
    initialValues: {
      currentPassword: "",
      password: "",
      passwordConfirmation: "",
    },

    onSubmit: () => {
      const data = {
        firstName: useData.firstName,
        lastName: useData.lastName,
        email: useData.email,
        password: changePassFormik.values.password,
      };

      handleDataUpdate(useData.id, data)
        .then((res) => {
          if (res.status === 200) {
            enqueueSnackbar("password changed successfully", {
              variant: "success",
            });
            localStorage.setItem("user", JSON.stringify(res.data.data));
            localStorage.setItem("password", data.password);
            setChangePasswordModal(false);
            changePassFormik.resetForm({});
          }
        })
        .catch((err) => {
          enqueueSnackbar(
            err.response.data.message
              ? err.response.data.message
              : "Failed to change password",
            {
              variant: "error",
            }
          );
        });
    },
    validationSchema: changePasswordValidation,
  });

  const handleChangeDetails = () => {
    setChangeDetailsModal(true);
  };

  const handleChangePassword = () => {
    setChangePasswordModal(true);
  };

  return (
    <Box p={5} className={classes.borderLeft}>
      <Box pb={4} className={classes.border}>
        <Box>
          <Box className={classes.headerButtons}>
            <Hidden smDown>
              <Box className={classes.header}>
                <p>PROFILE</p>
                <p>BILLING</p>
                <p>INVOICE HISTORY</p>
              </Box>
            </Hidden>
            <Hidden mdUp>
              <Button>
                <MenuIcon></MenuIcon>
              </Button>
            </Hidden>

            <Button
              variant="contained"
              disableElevation
              color="secondary"
              size="large"
              className={classes.deleteButton}
            >
              DELETE ACCOUNT
            </Button>
          </Box>
          <Box className={classes.details}>
            <Box className={classes.detailsLeft}>
              <p>PROFILE PICTURE</p>
              <img
                src="https://app.365dropship.com/assets/images/profile-example.jpg"
                className={classes.profilePicture}
                alt=""
              />
              <Button variant="contained" disableElevation color="primary">
                Upload
              </Button>
              <Button color="secondary" onClick={handleChangePassword}>
                Change Password
              </Button>
            </Box>
            <PersonalDetailsInput
              handleChangeDetails={handleChangeDetails}
              formik={formik}
              classes={classes}
            />
          </Box>
        </Box>
      </Box>
      <ChangeDetailsModal
        changeDetailsModal={changeDetailsModal}
        setChangeDetailsModal={setChangeDetailsModal}
        formik={formik}
        classes={classes}
      />

      <ChangePasswordModal
        changePasswordModal={changePasswordModal}
        setChangePasswordModal={setChangePasswordModal}
        changePassFormik={changePassFormik}
        classes={classes}
      />
    </Box>
  );
};

export default ProfileDetails;
