import { Button, Box, makeStyles } from "@material-ui/core";
import "./ProfileHeader.css";
import HelpButton from "../Header/HelpButton";
import { useHistory } from "react-router-dom";

const ProfileHeader = () => {
  const useStyles = makeStyles({
    header: {
      display: "flex",
      justifyContent: "space-evenly",
      width: "250px",
    },
    signOutBtn: {
      backgroundColor: "#e6eefc",
      color: "#4785ef",
      width: "132px",
      height: " 40px",
      "&:hover": {
        background: "#d2e1fb",
      },
    },
  });

  const classes = useStyles();

  const history = useHistory();
  const signOut = () => {
    localStorage.clear();
    history.push("/signin");
  };

  return (
    <div className="profile-header">
      <h3 className="profile-header__text">My Profile</h3>
      <Box mr={5} className={classes.header}>
        <Button
          variant="contained"
          color="primary"
          disableElevation
          className={classes.signOutBtn}
          onClick={signOut}
        >
          Sign out
        </Button>
        <HelpButton />
      </Box>
    </div>
  );
};

export default ProfileHeader;
