import ProfileHeader from "./ProfileHeader";
import "./Profile.css";
import ProfileDetails from "./ProfileDetails";

const Profile = () => {
  return (
    <div className="profile">
      <ProfileHeader />
      <ProfileDetails />
    </div>
  );
};

export default Profile;
