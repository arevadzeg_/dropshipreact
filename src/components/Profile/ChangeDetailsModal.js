import { Dialog, Box, TextField, Button } from "@material-ui/core";

const ChangeDetailsModal = ({
  changeDetailsModal,
  setChangeDetailsModal,
  formik,
  classes,
}) => {
  return (
    <Dialog
      open={changeDetailsModal}
      onClose={() => setChangeDetailsModal(false)}
    >
      <Box p={5} className={classes.modalStyles}>
        <h3>Please Enter your password</h3>
        <TextField
          name="password"
          type="password"
          value={formik.values.password}
          onChange={formik.handleChange}
        ></TextField>
        <Button
          color="secondary"
          disableElevation
          onClick={formik.handleSubmit}
        >
          Confirm
        </Button>
      </Box>
    </Dialog>
  );
};

export default ChangeDetailsModal;
