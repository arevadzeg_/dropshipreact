import { makeStyles } from "@material-ui/styles";

const useStyles = makeStyles({
  header: {
    display: "flex",
    height: "55px",
    alignItems: "center",
    width: "300px",
    justifyContent: "space-between",
    fontSize: "14px",
    color: "#303856",
    fontWeight: "bold",
  },
  headerButtons: {
    display: "flex",
    justifyContent: "space-between",
    alignItems: "center",
    padding: "5px 20px 5px 20px",
    boxShadow: "0 2px 4px 0 rgb(0 0 0 / 6%)",
  },

  border: {
    border: "1px solid #dcdde3",
    borderRadius: "5px",
  },

  details: {
    display: "flex",
    flexWrap: "wrap",
  },
  detailsLeft: {
    marginTop: "25px",
    flex: "500px 1",
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
    height: "350px",
    justifyContent: "space-between",
  },
  detailsRight: {
    marginTop: "25px",
    flex: "500px 1",
    flexDirection: "column",
    alignItems: "center",
    height: "350px",
    justifyContent: "space-between",
    display: "flex",
  },

  profilePicture: {
    borderRadius: "50%",
    height: "148px",
    width: "148px",
    border: "5px solid #61d5df",
    padding: "5px",
  },
  personalDetails: {
    display: "flex",
    flexDirection: "column",
    justifyContent: "space-between",
    height: "200px",
    width: "350px",
  },
  borderLeft: {
    borderLeft: "1px solid #dde3ee",
  },
  modalStyles: {
    display: "flex",
    justifyContent: "space-between",
    flexDirection: "column",
    height: "200px",
  },
  deleteButton: {
    backgroundColor: "#fff1f1",
    color: "#ff7b7b",
    "&:hover": {
      background: "#ffe4e4",
    },
  },
  changePassword: {
    height: "400px",
    width: "400px",
    display: "flex",
    justifyContent: "space-between",
    flexDirection: "column",
    padding: "40px",
  },
});

export default useStyles;
