import { Box, TextField, Button } from "@material-ui/core";

const PersonalDetailsInput = ({ handleChangeDetails, formik, classes }) => {
  return (
    <Box className={classes.detailsRight}>
      <p>PERSONAL DETAILS</p>
      <form className={classes.personalDetails}>
        <TextField
          label="First Name"
          variant="outlined"
          size="small"
          name="firstName"
          value={formik.values.firstName}
          onChange={formik.handleChange}
        ></TextField>
        <TextField
          label="Last Name"
          variant="outlined"
          size="small"
          name="lastName"
          onChange={formik.handleChange}
          value={formik.values.lastName}
        ></TextField>
        <TextField
          label="Email"
          variant="outlined"
          size="small"
          name="email"
          onChange={formik.handleChange}
          value={formik.values.email}
        ></TextField>
      </form>
      <Button
        variant="contained"
        disableElevation
        color="primary"
        onClick={handleChangeDetails}
      >
        Save Changes
      </Button>
    </Box>
  );
};

export default PersonalDetailsInput;
