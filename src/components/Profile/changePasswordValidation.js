import * as yup from "yup";

export const changePasswordValidation = yup.object().shape({
  currentPassword: yup
    .string()
    .required("required")
    .oneOf(
      [localStorage.getItem("password"), null],
      "Current Password is Incorect"
    ),
  password: yup.string().required("required").min(3),
  passwordConfirmation: yup
    .string()
    .required("required")
    .min(3)
    .oneOf([yup.ref("password"), null], "Passwords must match"),
});
