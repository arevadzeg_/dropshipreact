import { Dialog, TextField, Button } from "@material-ui/core";

const ChangePasswordModal = ({
  changePasswordModal,
  setChangePasswordModal,
  changePassFormik,
  classes,
}) => {
  return (
    <Dialog
      open={changePasswordModal}
      onClose={() => setChangePasswordModal(false)}
    >
      <form className={classes.changePassword}>
        <p>Change Password</p>
        <TextField
          label="Current Password"
          type="password"
          name="currentPassword"
          onChange={changePassFormik.handleChange}
          error={
            changePassFormik.touched.currentPassword &&
            changePassFormik.errors.currentPassword
              ? true
              : false
          }
          helperText={
            changePassFormik.touched.currentPassword &&
            changePassFormik.errors.currentPassword
          }
          autoComplete="on"
        ></TextField>
        <TextField
          label="New Password"
          type="password"
          name="password"
          error={
            changePassFormik.touched.password &&
            changePassFormik.errors.password
              ? true
              : false
          }
          helperText={
            changePassFormik.touched.password &&
            changePassFormik.errors.password
          }
          onChange={changePassFormik.handleChange}
          autoComplete="on"
        ></TextField>
        <TextField
          label="New Password"
          type="password"
          name="passwordConfirmation"
          error={
            changePassFormik.touched.passwordConfirmation &&
            changePassFormik.errors.passwordConfirmation
              ? true
              : false
          }
          helperText={
            changePassFormik.touched.passwordConfirmation &&
            changePassFormik.errors.passwordConfirmation
          }
          onChange={changePassFormik.handleChange}
          autoComplete="on"
        ></TextField>
        <Button onClick={changePassFormik.handleSubmit}>Change</Button>
      </form>
    </Dialog>
  );
};

export default ChangePasswordModal;
