import { Box, Button, Paper } from "@material-ui/core";
import ArrowBackIcon from "@material-ui/icons/ArrowBack";
import { makeStyles } from "@material-ui/styles";
import "./CheckOut.css";
const CheckOut = ({ total }) => {
  const useStyles = makeStyles({
    checkOutLayout: {
      display: "flex",
      justifyContent: "space-between",
      flexWrap: "wrap",
      "@media (max-width:1500px)": {
        flexDirection: "column",
        gap: "15px",
      },
    },
    rightLayout: {
      display: "flex",
      flex: "1 800px",
      justifyContent: "space-around",
      alignItems: "center",
      marginLeft: "30px",
      "@media (max-width:1500px)": {
        margin: 0,
        flex: "1 0",
      },
      "@media (max-width:1200px)": {
        flexDirection: "column",
        alignItems: "stretch",
        gap: "5px",
      },
    },

    leftLayout: {
      marginRight: "150px",
    },

    buttonFont: {
      fontSize: "14px",
      fontWeight: 600,
    },
  });

  const classes = useStyles();

  return (
    <Box m={4}>
      <Paper>
        <Box p={3} className={classes.checkOutLayout}>
          <Button
            startIcon={<ArrowBackIcon />}
            color="primary"
            variant="contained"
            disableElevation
            className={classes.buttonFont}
          >
            continue shopping
          </Button>
          <Box className={classes.rightLayout}>
            <Box className={classes.rightLayout}>
              <div className="checkout__text">
                <span>BALANCE:</span>
                <span>$0.00</span>
              </div>
              <div className="checkout__text">
                <span>ITEMS TOTAL:</span>
                <span>${total}</span>
              </div>
              <div className="checkout__text">
                <span>SHIPPING TOTAL:</span>
                <span>$0.00</span>
              </div>
              <div className="checkout__text">
                <span>ORDER TOTAL:</span>
                <span>${total}</span>
              </div>
              <Button
                color="primary"
                variant="contained"
                disableElevation
                className={classes.buttonFont}
              >
                checkout
              </Button>
            </Box>
          </Box>
        </Box>
      </Paper>
    </Box>
  );
};

export default CheckOut;
