import { Box, Typography, makeStyles } from "@material-ui/core";
import { Link } from "react-router-dom";

const useStyles = makeStyles({
  centerEverything: {
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
    flexDirection: "column",
    height: "500px",
  },
  textWidth: {
    maxWidth: "500px",
    textAlign: "center",
  },
});

const InventoryNoItem = () => {
  const classes = useStyles();
  return (
    <Box className={classes.centerEverything}>
      <img
        alt=""
        src="https://app.365dropship.com/assets/images/svg/empty_inventory.svg"
      />
      <Typography className={classes.textWidth}>
        Your inventory is currently empty, to add products to sync, choose from
        the <Link to="/catalog">catalog</Link>
      </Typography>
    </Box>
  );
};

export default InventoryNoItem;
