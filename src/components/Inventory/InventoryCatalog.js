import { useState } from "react";
import { Box, Hidden, makeStyles } from "@material-ui/core";
import { useSnackbar } from "notistack";
import InventoryNoItem from "./InventoryNoItem";
import { useHistory } from "react-router-dom";
import Modal from "../Catalog/Modal";
import { removeProductFromCart } from "../API/api";
import { useDispatch, useSelector } from "react-redux";
import {
  fillUpDisplay,
  filterDisplay,
  closeModal,
} from "../Redux/ProductsReducer/productsActions";
import {
  Table,
  TableContainer,
  Paper,
  TableHead,
  TableRow,
  TableCell,
  TableBody,
} from "@material-ui/core";
import CartItem from "./CartItem";
import CheckOut from "./CheckOut";

const useStyles = makeStyles({
  catalog: {
    marginTop: "113px",
    overflowY: "scroll",
    height: "calc(100vh - 113px)",
    borderLeft: "1px solid #e0e0e0",
  },
  cart: {
    display: "flex",
    flexDirection: "column",
  },

  checkBox: {
    width: "90px",
    color: "#303856",
    fontFamily: "Helvetica",
    fontSize: "14px",
    fontWeight: "600",
    "@media (max-width:780px)": {
      fontSize: "12px",
    },
  },

  font: {
    color: "#303856",
    fontFamily: "Helvetica",
    fontSize: "14px",
    fontWeight: "600",
    "@media (max-width:780px)": {
      fontSize: "12px",
    },
  },
  tableHeader: {
    backgroundColor: "#ECEDF5",
  },

  postitionRelative: {
    position: "relative",
  },

  "@global": {
    body: {
      overflow: "hidden",
    },
  },
});

const InventoryCatalog = ({ backdropOpen }) => {
  const classes = useStyles();
  const { enqueueSnackbar } = useSnackbar();
  const history = useHistory();
  const dispatch = useDispatch();
  const cart = useSelector((state) => state.products.displayProducts);
  const [totalValue, setTotalValue] = useState(0);

  const removeProduct = () => {
    const queryString = new URLSearchParams(history.location.search);
    removeProductFromCart(queryString.get("id"))
      .then(() => {
        enqueueSnackbar("Items removed", {
          variant: "success",
          autoHideDuration: 2000,
        });
        const fileteredCart = cart.filter(
          (item) => item.id !== Number(queryString.get("id"))
        );
        dispatch(fillUpDisplay(fileteredCart));
        dispatch(filterDisplay(fileteredCart));
        history.push(`${history.location.pathname}?${queryString.toString()}`);
        dispatch(closeModal());
      })
      .catch((err) => {
        enqueueSnackbar(err.response.data.message, {
          variant: "error",
          autoHideDuration: 2000,
        });
      });
  };

  return (
    <Box className={classes.catalog}>
      <Box>
        <TableContainer component={Paper}>
          <Table>
            <TableHead>
              <TableRow className={classes.tableHeader}>
                <TableCell className={classes.checkBox}>CHECKED</TableCell>
                <TableCell className={classes.font}>ITEM DESCRIPTION</TableCell>
                <Hidden smDown>
                  <TableCell></TableCell>
                </Hidden>
                <Hidden smDown>
                  <TableCell className={classes.font}>PRICE</TableCell>
                </Hidden>
                <TableCell className={classes.font}>QUANTITY</TableCell>
                <TableCell className={classes.font}>TOTAL COST</TableCell>
              </TableRow>
            </TableHead>
            {cart.length > 0 && (
              <TableBody className={classes.postitionRelative}>
                {cart.map((item) => {
                  return (
                    <CartItem
                      qty={item.qty}
                      image={item.image}
                      title={item.title}
                      price={item.price}
                      id={item.id}
                      key={item.id}
                      setTotalValue={setTotalValue}
                    />
                  );
                })}
              </TableBody>
            )}
          </Table>
        </TableContainer>
        {!backdropOpen && cart.length === 0 && <InventoryNoItem />}
        {cart.length > 0 && <CheckOut total={totalValue} />}

        <Modal removeProduct={removeProduct} />
      </Box>
    </Box>
  );
};

export default InventoryCatalog;
