import { Box, makeStyles, Backdrop } from "@material-ui/core";
import InventoryHeader from "./InventoryHeader";
import InventoryCatalog from "./InventoryCatalog";
import { useState, useEffect } from "react";
import { getUserCart } from "../API/api";
import { Snackbar } from "@material-ui/core";
import MuiAlert from "@material-ui/lab/Alert";
import { useHistory } from "react-router";
import { fillUpDisplay } from "../Redux/ProductsReducer/productsActions";
import { useDispatch } from "react-redux";

const useStyles = makeStyles({
  main: {
    flexGrow: "1",
    width: "calc(100% - 313px)",
  },
  backdrop: {
    zIndex: "5",
    "@media (min-width:1070px)": {
      left: "55px",
    },
  },
});

const InventoryMain = () => {
  const [backdropOpen, setBackdropOpen] = useState(true);
  const dispatch = useDispatch();
  const history = useHistory();

  const [snackBarOpen, setSnackBarOpen] = useState(false);
  const closeSnackBar = () => {
    setSnackBarOpen(false);
  };

  function Alert(props) {
    return <MuiAlert elevation={6} variant="filled" {...props} />;
  }

  useEffect(() => {
    getUserCart()
      .then((response) => {
        setBackdropOpen(true);
        if (response) {
          dispatch(fillUpDisplay(response.data.data.cartItem.items));
        }
      })
      .catch()
      .finally(() => setBackdropOpen(false));
  }, [dispatch, history]);

  const classes = useStyles();
  return (
    <Box className={classes.main}>
      <Backdrop className={classes.backdrop} open={backdropOpen}>
        {backdropOpen && (
          <img
            alt=""
            src="https://i.pinimg.com/originals/ed/2e/c8/ed2ec87072fd6cc5c42149ae082b2d51.gif"
            style={{ filter: "brightness(0.5)" }}
          />
        )}
      </Backdrop>
      <InventoryHeader setSnackBarOpen={setSnackBarOpen} />
      <InventoryCatalog backdropOpen={backdropOpen} />
      <Snackbar
        open={snackBarOpen}
        autoHideDuration={1000}
        onClose={closeSnackBar}
      >
        <Alert>Product Removed Successfully</Alert>
      </Snackbar>
    </Box>
  );
};

export default InventoryMain;
