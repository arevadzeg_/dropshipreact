import { Box, Button, Typography, Hidden } from "@material-ui/core";
import SearchInput from "../Header/SearchInput";
import HelpButton from "../Header/HelpButton";
import { removeProductFromCart } from "../API/api";
import { useSelector, useDispatch } from "react-redux";
import DeleteIcon from "@material-ui/icons/Delete";
import {
  fillUpDisplay,
  filterDisplay,
} from "../Redux/ProductsReducer/productsActions";
import { deselectProducts } from "../Redux/ProductsReducer/productsActions";
import useStyles from "../Header/searchStyles";
import FilterDrawer from "../Filter/FilterDrawer";
import classNames from "classnames";
import SelectAndDeselectButton from "../Buttons/SelectAndDeselectButton";
import SelectAllBtn from "../Buttons/SelectAllBtn";

const InventorySearch = ({ setSnackBarOpen }) => {
  const classes = useStyles();
  const dispatch = useDispatch();
  const cart = useSelector((state) => state.products.displayProducts);
  const selectedItems = useSelector((state) => state.products.selectedProducts);

  const deselectAll = () => {
    dispatch(deselectProducts());
  };

  const handleProductRemove = async () => {
    const filteredCart = cart.filter((item) => {
      return !selectedItems.includes(Number(item.id));
    });

    dispatch(fillUpDisplay(filteredCart));
    dispatch(filterDisplay(filteredCart));
    setSnackBarOpen(true);

    for (const item of selectedItems) {
      await removeProductFromCart(item);
    }

    deselectAll();
  };

  return (
    <Box pl={{ xs: "20px", md: "40px" }} className="search-sort__search">
      <Box className={classNames(classes.flexBox, classes.flexRightBox)}>
        <FilterDrawer />
        <SelectAllBtn />

        <SelectAndDeselectButton />

        <Hidden xsDown>
          <Box className="search-sort__selected-number" pl="20px" pt="2px">
            <Hidden mdDown>
              <Typography
                component="span"
                className={classes.productQuantityText}
              >
                selected {selectedItems.length} out of{" "}
              </Typography>
            </Hidden>

            <Typography
              component="span"
              className={classes.productQuantityText}
            >
              {cart ? cart.length : 0} products
            </Typography>
          </Box>
        </Hidden>

        <Hidden mdDown>
          {selectedItems.length > 0 && (
            <Button
              color="primary"
              variant="contained"
              disableElevation
              onClick={deselectAll}
            >
              Clear
            </Button>
          )}
        </Hidden>
      </Box>
      <Box className={classNames(classes.flexBox, classes.flexLeftBox)}>
        <Box className={classes.searchInput}>
          <SearchInput />
        </Box>

        <Hidden mdDown>
          <Button
            variant="contained"
            color="primary"
            disableElevation
            onClick={handleProductRemove}
          >
            Remove from my inventory
          </Button>
        </Hidden>
        <Hidden lgUp>
          <Button
            color="primary"
            variant="contained"
            disableElevation
            className={classes.addSmallBtn}
            onClick={handleProductRemove}
          >
            <DeleteIcon />
          </Button>
        </Hidden>

        <HelpButton />
      </Box>
    </Box>
  );
};

export default InventorySearch;
