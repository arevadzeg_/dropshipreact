import Filters from "../Filter/Filters";
import InventoryMain from "./InventoryMain";

const Inventory = () => {
  return (
    <>
      <Filters />
      <InventoryMain />
    </>
  );
};

export default Inventory;
