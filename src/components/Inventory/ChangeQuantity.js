import {
  Box,
  Badge,
  Collapse,
  makeStyles,
  Button,
  Dialog,
  DialogTitle,
  ButtonGroup,
} from "@material-ui/core";
import ShoppingCartOutlinedIcon from "@material-ui/icons/ShoppingCartOutlined";
import { useState, useEffect, useRef } from "react";
import AddIcon from "@material-ui/icons/Add";
import RemoveIcon from "@material-ui/icons/Remove";
import { updateCart, removeProductFromCart } from "../API/api";
import CancelIcon from "@material-ui/icons/Cancel";
import DeleteIcon from "@material-ui/icons/Delete";
import { useDispatch, useSelector } from "react-redux";
import { fillUpDisplay } from "../Redux/ProductsReducer/productsActions";
const useStyles = makeStyles({
  buttonStyle: {
    width: "25px",
    minWidth: "25px",
    height: "25px",
    marginRight: "5px",
  },
  icon: {
    marginLeft: "15px",
  },
});

const ChangeQuantity = ({ id, qty, setTotal, price, setTotalValue }) => {
  const classes = useStyles();
  const [quantity, setQuantity] = useState(Number(qty));

  const [editShown, setEditShown] = useState(false);
  const [dialogOpen, setDialogOpen] = useState(false);
  const cart = useSelector((state) => state.products.displayProducts);
  const dispatch = useDispatch();
  const initalRender = useRef(false);

  useEffect(() => {
    if (initalRender.current) {
      const delay = setTimeout(() => {
        updateCart(id, quantity).then((data) => {
          if (data.data.data.cartItem.items.length > 0) {
            const newTotal = data.data.data.cartItem.items.reduce(
              (accumulator, currentValue) =>
                (accumulator +=
                  Number(currentValue.price) * Number(currentValue.qty)),
              0
            );
            setTotalValue(newTotal);
          }
        });
      }, [1000]);

      return () => {
        clearTimeout(delay);
      };
    }
    initalRender.current = true;
  }, [quantity, id, setTotalValue]);

  const closeDialog = () => {
    setDialogOpen(false);
  };

  const removeSingleProduct = () => {
    removeProductFromCart(id);
    const fileteredCart = cart.filter((item) => item.id !== id);
    dispatch(fillUpDisplay(fileteredCart));
  };

  const increaseQuantity = () => {
    setQuantity(quantity + 1);
    setTotal((quantity + 1) * price);
  };
  const decreaseQuantity = () => {
    if (quantity - 1 > 0) {
      setQuantity(quantity - 1);
      setTotal((quantity - 1) * price);
    } else {
      setDialogOpen(true);
    }
  };

  useEffect(() => {
    if (cart.length > 0) {
      const newTotal = cart.reduce(
        (accumulator, currentValue) =>
          (accumulator +=
            Number(currentValue.price) * Number(currentValue.qty)),
        0
      );
      setTotalValue(newTotal);
    }
  }, [cart, setTotalValue]);

  return (
    <Box
      onMouseOver={() => {
        setEditShown(true);
      }}
      onMouseOut={() => {
        setEditShown(false);
      }}
      className="edit-quantity"
    >
      <Badge badgeContent={quantity} color="secondary" className={classes.icon}>
        <ShoppingCartOutlinedIcon />
      </Badge>
      <Collapse in={editShown}>
        <Button
          color="secondary"
          variant="contained"
          onClick={increaseQuantity}
          disableElevation
          className={classes.buttonStyle}
        >
          <AddIcon />
        </Button>
        <Button
          color="secondary"
          variant="contained"
          onClick={decreaseQuantity}
          disableElevation
          className={classes.buttonStyle}
        >
          <RemoveIcon />
        </Button>
      </Collapse>
      <Dialog open={dialogOpen} onClose={closeDialog}>
        <Box p={5}>
          <DialogTitle>
            Are you sure you want to remove product from the cart ?
          </DialogTitle>
          <Box ml={3} mt={2}>
            <ButtonGroup>
              <Button
                color="secondary"
                disableElevation
                startIcon={<DeleteIcon />}
                onClick={removeSingleProduct}
              >
                Yes
              </Button>
              <Button
                variant="contained"
                color="secondary"
                disableElevation
                startIcon={<CancelIcon />}
                onClick={closeDialog}
              >
                No
              </Button>
            </ButtonGroup>
          </Box>
        </Box>
      </Dialog>
    </Box>
  );
};

export default ChangeQuantity;
