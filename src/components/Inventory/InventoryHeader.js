import { Box } from "@material-ui/core";
import InventorySearch from "./InventorySearch";
import Sort from "../Header/Sort";

const InventoryHeader = ({ setSnackBarOpen }) => {
  return (
    <Box className="search-sort">
      <InventorySearch setSnackBarOpen={setSnackBarOpen} />
      <Sort />
    </Box>
  );
};

export default InventoryHeader;
