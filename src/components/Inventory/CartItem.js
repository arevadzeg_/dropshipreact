import { TableRow, TableCell, Box, Hidden } from "@material-ui/core";
import { useHistory } from "react-router";
import { makeStyles } from "@material-ui/styles";
import { useState } from "react";
import ChangeQuantity from "./ChangeQuantity";
import ItemCheckBox from "../Catalog/ItemCheckBox";
import { useSelector } from "react-redux";

const CartItem = ({ image, qty, price, id, title, setTotalValue }) => {
  const useStyles = makeStyles({
    img: {
      width: "120px",
      height: "120px",
      backgroundImage: `url('${image}')`,
      backgroundSize: "contain",
      backgroundPosition: "center",
      backgroundRepeat: "no-repeat",
    },
    checkBoxPosition: {
      position: "relative",
      transform: "translate(-35px,-35px)",
    },

    modalPointer: {
      cursor: "pointer",
    },
    checkBox: {
      width: "90px",
    },
    positionRelatilve: {
      position: "relative",
    },
  });

  const history = useHistory();
  const selectedItems = useSelector((state) => state.products.selectedProducts);

  const handleOpenModal = (e) => {
    const queryString = new URLSearchParams(history.location.search);
    queryString.set("id", id);
    history.push(`${history.location.pathname}?${queryString.toString()}`);
  };

  const [total, setTotal] = useState(price * qty);

  const classes = useStyles();
  return (
    <TableRow
      className={`margin_right ${
        selectedItems.includes(id) ? "checked_shadow" : ""
      }`}
    >
      <TableCell className={classes.checkBox}>
        <Box className={classes.checkBoxPosition}>
          <ItemCheckBox id={id} shown={true} />
        </Box>
      </TableCell>
      <TableCell align="left">
        <Box onClick={handleOpenModal} className={classes.modalPointer}>
          <div className={classes.img}></div>
          <Hidden mdUp>
            <p>{title}</p>
          </Hidden>
        </Box>
      </TableCell>
      <Hidden smDown>
        <TableCell align="left">{title}</TableCell>
      </Hidden>
      <Hidden smDown>
        <TableCell align="left">{price}</TableCell>
      </Hidden>
      <TableCell align="left" className={classes.positionRelatilve}>
        {
          <ChangeQuantity
            qty={qty}
            id={id}
            setTotal={setTotal}
            price={price}
            setTotalValue={setTotalValue}
          />
        }
      </TableCell>
      <TableCell align="left">{total}</TableCell>
    </TableRow>
  );
};

export default CartItem;
