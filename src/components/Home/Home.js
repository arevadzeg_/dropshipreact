import "./Home.css";
import HomeHeader from "./HomeHeader.js";

const Home = () => {
  return (
    <div
      className="home"
      style={{
        backgroundImage:
          "url(https://mk0q365dropshipe482k.kinstacdn.com/wp-content/uploads/2020/06/hero-1.jpg)",
      }}
    >
      <HomeHeader />

      <div className="home__text">
        <img
          alt=""
          className="home__logo"
          src="https://mk0q365dropshipe482k.kinstacdn.com/wp-content/uploads/2020/06/356Logo.svg"
        />
        <div>WE GOT YOUR SUPPLY CHAIN COVERED</div>
        <div>365 DAYS A YEAR!</div>
      </div>
    </div>
  );
};

export default Home;
