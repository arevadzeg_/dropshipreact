import React, { useEffect, useLayoutEffect, useRef } from "react";
import "./FiltersSlider.css";
import { makeStyles } from "@material-ui/core/styles";
import Slider from "@material-ui/core/Slider";
import { useDispatch, useSelector } from "react-redux";
import filteringWays from "../filteringWays";
import {
  changeSliderValues,
  changeSliderMinMax,
  changeDisplay,
} from "../Redux/ProductsReducer/productsActions";

const useStyles = makeStyles({
  root: {
    width: "100%",
  },
});

export default function FiltersSlider() {
  const classes = useStyles();

  const dispatch = useDispatch();
  const allProducts = useSelector((state) => state.products.allProducts);

  const filters = useSelector((state) => state.products.filters);
  const minMax = useSelector((state) => state.products.sliderMinMax);
  const value = useSelector((state) => state.products.sliderValues);

  const testTest = filteringWays(filters, allProducts);

  useEffect(() => {
    if (allProducts.length > 0 && minMax[0] === 0) {
      const workArray = testTest;
      if (workArray.length > 0) {
        const min = workArray.reduce((max, item) => {
          return item.price < max.price ? item : max;
        }).price;

        const max = workArray.reduce((max, item) => {
          return item.price > max.price ? item : max;
        }).price;

        dispatch(changeSliderValues([min, max]));
        dispatch(changeSliderMinMax([min, max]));
      } else {
        dispatch(changeSliderValues([0, 0]));
        dispatch(changeSliderMinMax([0, 0]));
      }
    }
    if (minMax[1] !== 0) {
      dispatch(changeSliderValues(value));
      dispatch(changeSliderMinMax(minMax));
    }
  }, [allProducts, dispatch, minMax, value]);

  useLayoutEffect(() => {
    if (allProducts.length > 0) {
      const workArray = filteringWays(filters, allProducts);
      if (workArray.length > 0) {
        const min = workArray.reduce((max, item) => {
          return item.price < max.price ? item : max;
        }).price;

        const max = workArray.reduce((max, item) => {
          return item.price > max.price ? item : max;
        }).price;

        dispatch(changeSliderValues([min, max]));
        dispatch(changeSliderMinMax([min, max]));
      }
    }
  }, [filters, allProducts, dispatch]);

  const firstRender = useRef(false);

  useLayoutEffect(() => {
    if (firstRender.current) {
      const delay = setTimeout(() => {
        const filteredArray = filteringWays(filters, allProducts).filter(
          (item) => item.price >= value[0] && item.price <= value[1]
        );
        dispatch(changeDisplay(filteredArray));
      }, 500);

      return () => {
        clearTimeout(delay);
      };
    }
    firstRender.current = true;
  }, [value, allProducts, dispatch, filters]);

  const handleChange = (event, newValue) => {
    dispatch(changeSliderValues(newValue));
  };

  return (
    <div className={classes.root}>
      <p>Price Range</p>
      <Slider
        onChange={handleChange}
        aria-labelledby="range-slider"
        max={minMax ? minMax[1] : 0}
        min={minMax ? minMax[0] : 0}
        value={value}
      />
      <div className="filters__slider-numbers">
        <span className="filters__price-text">
          <span>$</span>
          <span>{value[0]}</span>
        </span>

        <span className="filters__price-text">
          <span>$</span>
          <span>{value[1]}</span>
        </span>
      </div>
    </div>
  );
}
