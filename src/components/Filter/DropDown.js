import "./DropDown.css";
import { useState } from "react";
import { Link, useLocation } from "react-router-dom";

function DropDown({ className, text }) {
  const [selectOpen, setSelectOpen] = useState(false);
  const location = useLocation();
  const handleSelectOpen = () => {
    setSelectOpen(!selectOpen);
  };

  return (
    <>
      <div className={className} onClick={handleSelectOpen}>
        <p>{text}</p>
        <i className="fas fa-angle-down"></i>
      </div>

      {selectOpen &&
        className === "filters__dropdown filters__dropdown--categories" &&
        localStorage.categories && (
          <div className="filter__category">
            <Link to={`/catalog${location.search}`}>
              <div>...</div>
            </Link>
            {JSON.parse(localStorage.categories).map((category) => {
              return (
                <Link
                  to={`/catalog/${category}${location.search}`}
                  key={category}
                >
                  <div key={category}>{category}</div>
                </Link>
              );
            })}
          </div>
        )}
    </>
  );
}

export default DropDown;
