import "./filters.css";
import DropDown from "./DropDown.js";
import DropDownShipping from "./DropDownShipping.js";
import FiltersSlider from "./FiltersSlider";
import { useEffect, useState } from "react";
import axios from "axios";
import { Button } from "@material-ui/core";
import ArrowBackIcon from "@material-ui/icons/ArrowBack";
import { makeStyles } from "@material-ui/styles";

const Filters = ({ width ,handleFilterOpen}) => {
  const [countries, setCountries] = useState();

  const useStyles = makeStyles({
    backButton: {
      width: "90%",
      margin: "25px 25px 0 25px",
      color: "#fff",
      backgroundColor: "#49547D",
      "&:hover": {
        backgroundColor: "#696f86",
      },
    },
  });

  const classes = useStyles();

  const getAllcountries = async () => {
    const data = await axios({ url: "https://restcountries.eu/rest/v2/all" });
    setCountries(data.data);
  };

  useEffect(() => {
    getAllcountries();
  }, []);

  return (
    <div className={`filters ${Number(width) < 959 ? "drawerFilters" : ""}`}>
      <DropDown
        className="filters__dropdown filters__dropdown--niche"
        text="Choose Niche"
      />
      <DropDown
        className="filters__dropdown filters__dropdown--categories"
        text="Choose Category"
      />

      {width && (
        <Button
          variant="contained"
          disableElevation
          startIcon={<ArrowBackIcon />}
          onClick={handleFilterOpen}
          className={classes.backButton}
        >
          Back
        </Button>
      )}

      <div className="filters__shiping">
        <DropDownShipping
          className="filters__ship-from"
          text="Ship From"
          countries={countries}
        />
        <DropDownShipping className="filters__ship-to" text="Ship To" />
        <DropDownShipping
          className="filters__supplier "
          text="Select supplier"
        />
        <FiltersSlider

        />
        <button className="filters__btn">RESET FILTERS</button>
      </div>
    </div>
  );
};

export default Filters;
