import { useState } from "react";
import Filters from "./Filters";
import "./FilterDrawer.css";
import { makeStyles, Button, Hidden, Drawer } from "@material-ui/core";
import { useEffect } from "react";

const useStyles = makeStyles({
  zIndex: {
    zIndex: "70000 !important",
  },
  productQuantityText: {
    fontSize: "12px",
    color: "#29304b",
    fontWeight: "600",
  },
  selectAllBtn: {
    height: "40px",
    minWidth: "40px",
    fontSize: "18px",
  },
});

const FilterDrawer = ({ breakPoint }) => {
  const [filterOpen, setFilterOpen] = useState(false);

  const classes = useStyles();

  const handleFilterOpen = () => {
    setFilterOpen(!filterOpen);
  };

  const [width, setWidth] = useState(window.innerWidth);

  const windowResize = () => {
    setWidth(window.innerWidth);
  };

  useEffect(() => {
    window.addEventListener("resize", windowResize);
    return () => window.removeEventListener("resize", windowResize);
  }, []);

  return (
    <>
      <Hidden only={["md", "lg", "xs", "xl"]}>
        <Button
          variant="contained"
          color="primary"
          disableElevation
          onClick={handleFilterOpen}
        >
          Filter
        </Button>
      </Hidden>
      <Hidden smUp>
        <Button
          variant="contained"
          color="primary"
          disableElevation
          onClick={handleFilterOpen}
          className={classes.selectAllBtn}
        >
          <i className="fas fa-filter"></i>
        </Button>
      </Hidden>

      <Drawer
        open={filterOpen}
        anchor="top"
        onClose={handleFilterOpen}
        className={`${classes.zIndex} filterDrawer`}
      >
        <Filters
          width={width}
          handleFilterOpen={handleFilterOpen}
          breakPoint={breakPoint}
        />
      </Drawer>
    </>
  );
};

export default FilterDrawer;
