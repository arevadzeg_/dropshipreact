import "./DropDownShipping.css";
import { useState } from "react";

const DropDownShipping = ({ className, text, countries }) => {
  const [selectOpen, setSelectOpen] = useState(false);

  const handleSelectOpen = () => {
    setSelectOpen(!selectOpen);
  };
  return (
    <div>
      <div className={`filters__select ${className}`}>
        <p onClick={handleSelectOpen} className="filters__select-text">
          {text} <i className="fas fa-caret-down"></i>
        </p>
        {selectOpen && countries && (
          <div className="filters__ship-countries">
            {countries.map((country) => {
              return (
                <div className="dropdown__display" key={country.alpha2Code}>
                  <input type="checkbox" id={country.alpha2Code} />
                  <label htmlFor={country.alpha2Code}>
                    <img src={country.flag} alt="flag" />
                    <span>{country.name}</span>
                  </label>
                </div>
              );
            })}
          </div>
        )}
      </div>
    </div>
  );
};

export default DropDownShipping;
