export default function sort(way, array) {
  if (way === "asc") {
    array.sort((a, b) => {
      return a.price - b.price;
    });
  } else if (way === "desc") {
    array.sort((a, b) => {
      return b.price - a.price;
    });
  } else if (way === "descName") {
    array.sort((a, b) => {
      return b.title.charCodeAt(0) - a.title.charCodeAt(0);
    });
  } else if (way === "ascName") {
    array.sort((a, b) => {
      return a.title.charCodeAt(0) - b.title.charCodeAt(0);
    });
  }
}
