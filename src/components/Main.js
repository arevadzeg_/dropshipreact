import Catalog from "./Catalog/Catalog";
import Header from "./Header/Header";
import "./Main.css";
import React, { useEffect } from "react";
import { useDispatch } from "react-redux";
import { fillUpDisplay } from "./Redux/ProductsReducer/productsActions";
import { getAllProducts } from "./API/api";

function Main({ breakPoint }) {
  const dispatch = useDispatch();

  useEffect(() => {
    getAllProducts().then((response) =>
      dispatch(fillUpDisplay(response.data.data))
    );
  }, [dispatch]);

  return (
    <main className="main">
      <Header breakPoint={breakPoint} />

      <Catalog />
    </main>
  );
}

export default Main;
