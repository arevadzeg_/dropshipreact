import "./App.css";
import Nav from "./components/Nav/nav.js";
import Filter from "./components/Filter/Filters";
import Main from "./components/Main";
import useMediaQuery from "@material-ui/core/useMediaQuery";
import TemporaryComponent from "./components/TemporaryComponent/TemporaryComponent";
import SignUp from "./components/Authorization/SignUp";
import Authorization from "./components/Authorization/Authorization";
import React, { useEffect, useState } from "react";
import { Switch, Route, useLocation, useHistory } from "react-router-dom";
import Inventory from "./components/Inventory/Inventory";
import Home from "./components/Home/Home";
import LogIn from "./components/Authorization/SignIn";
import Profile from "./components/Profile/Profile";
import { useDispatch, useSelector } from "react-redux";
import {
  changeFilters,
  closeModal,
} from "./components/Redux/ProductsReducer/productsActions";
import { openModal } from "./components/Redux/ProductsReducer/productsActions";
import { filterDisplay } from "./components/Redux/ProductsReducer/productsActions";
import { getSingleProduct } from "./components/API/api";
import BurgerM from "./components/Header/BurgerM";
import BurgerMenu from "./components/Header/BurgerMenu";
import { useSnackbar } from "notistack";

function App() {
  const breakPoint = useMediaQuery("(min-width:950px)");
  const location = useLocation();
  const dispatch = useDispatch();
  const history = useHistory();
  const [changeInFilter, setChangeInFilter] = useState({
    sort: "",
    searchQuery: "",
  });
  const [drawerState, setDrawerState] = useState(false);
  const handleDrawerOpen = () => {
    setDrawerState(!drawerState);
  };
  const { enqueueSnackbar } = useSnackbar();

  useEffect(() => {
    const url = new URLSearchParams(location.search);

    const newFilters = {
      sort: url.get("sort") ? url.get("sort") : "",
      searchQuery: url.get("search") ? url.get("search") : "",
    };

    if (
      newFilters.sort !== changeInFilter.sort ||
      newFilters.searchQuery !== changeInFilter.searchQuery
    ) {
      setChangeInFilter(newFilters);
    }
  }, [location.search, changeInFilter]);

  useEffect(() => {
    dispatch(changeFilters(changeInFilter));
  }, [changeInFilter, dispatch]);

  useEffect(() => {
    const queryString = new URLSearchParams(history.location.search);
    if (queryString.get("id")) {
      dispatch(openModal(true));
      getSingleProduct(queryString.get("id"))
        .then((response) => {
          dispatch(openModal(response.data.data));
        })
        .catch((err) => {
          if (err.response.status === 404) {
            dispatch(closeModal());
            enqueueSnackbar("Product No Longer Exists", {
              variant: "error",
            });
            history.goBack();
          }
        });
    }
  }, [history.location.search, dispatch, enqueueSnackbar, history]);

  const productFilters = useSelector((state) => state.products.filters);
  const allProducts = useSelector((state) => state.products.allProducts);

  useEffect(() => {
    if (allProducts.length > 0) {
      dispatch(filterDisplay(productFilters));
    }
  }, [productFilters, allProducts, dispatch]);

  return (
    <div className="app">
      <Switch>
        <Route exact path="/">
          <Home />
        </Route>
        <Route exact path="/signup">
          <Authorization component={<SignUp />} />
        </Route>
        <Route exact path="/signin">
          <Authorization component={<LogIn />} />
        </Route>

        <Route>
          <Nav />
          <BurgerM
            handleDrawerOpen={handleDrawerOpen}
            drawerState={drawerState}
          />
          <BurgerMenu
            handleDrawerOpen={handleDrawerOpen}
            drawerState={drawerState}
          ></BurgerMenu>

          <Route exact path="/profile">
            <Profile where="Profile" />
          </Route>
          <Route exact path="/dashboard">
            <TemporaryComponent where="Dashboard" />
          </Route>

          <Route path="/catalog">
            <Filter breakPoint={breakPoint} />
            <Main breakPoint={breakPoint} />
          </Route>

          <Route exact path="/inventory">
            <Inventory />
          </Route>
          <Route exact path="/cart">
            <TemporaryComponent where="Shopping Cart" />
          </Route>
          <Route path="/orders">
            <TemporaryComponent where="Orders" />
          </Route>
          <Route path="/transactions">
            <TemporaryComponent where="Transactions" />
          </Route>
          <Route path="/store">
            <TemporaryComponent where="Stores" />
          </Route>
        </Route>
      </Switch>
    </div>
  );
}

export default App;
